package org.dimensinfin.virtualregatta.routecalculator.windmap;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceFileNotFound;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;

import static org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler.WIND_DATA_DATE_FORMAT;

public class WindMapHandlerTest {
	private static final String TEST_GRIB_DATA_DIRECTORY = "/home/adam/Development/VORG/VRRouteCalculator/src/test/resources/grib-data/";
	private static final String TEST_GRIB_DATA_FILE = "20200428_GFS_winddata.json";
	private static final String TEST_MAP_DATA_DATE_REFERENCE = LocalDateTime.now().format( DateTimeFormatter.ofPattern( WIND_DATA_DATE_FORMAT ) );
	private static final double TEST_LATITUDE = 18.0;
	private static final double TEST_LONGITUDE = 26.5;
	private static final double TEST_LATITUDE_OUTOFMAP = -18.0;
	private static final double TEST_LONGITUDE_OUTOFMAP = -26.5;

	@Test
	public void buildComplete() throws WindResourceFileNotFound {
		final WindMapHandler mapHandler = new WindMapHandler.Builder().build();
		Assertions.assertNotNull( mapHandler );
	}

//	//	@Test
//	public void checkFileStructure() throws IOException {
//		final int numskip = 0;
//		final int gridid = 0;
//		final String filename = TEST_GRIB_DATA_DIRECTORY + TEST_GRIB_DATA_FILE;
//		System.out.println( "Reading file " + filename + ", file structure " + numskip + ":" );
//		// Open GRIB2 file and read it
//		InputStream inputstream;
//		inputstream = Files.newInputStream( Paths.get( filename ) );
//		RandomAccessGribFile gribFile = new RandomAccessGribFile( "testdata", filename );
//		gribFile.importFromStream( inputstream, numskip );
//
//		// Get identification information
//		GribSection1 section1 = gribFile.getSection1();
//		System.out.println( "Date: " + String.format( "%02d", section1.day ) + "." + String.format( "%02d", section1.month ) + "." + section1.year );
//		System.out.println( "Time: " + String.format( "%02d", section1.hour ) + ":" + String.format( "%02d", section1.minute ) + "." + String
//				.format( "%02d", section1.second ) );
//		System.out.println( "Generating centre: " + section1.generatingCentre );
//		// Get product information
//		ProductDefinitionTemplate40 productDefinition = (ProductDefinitionTemplate40) gribFile.getProductDefinitionTemplate();
//		System.out.println( "Forecast time: " + productDefinition.forecastTime );
//		System.out.println( "Parameter category: " + productDefinition.parameterCategory );
//		System.out.println( "Parameter number: " + productDefinition.parameterNumber );
//
//		// Get grid information
//		GridDefinitionTemplate30 gridDefinition = (GridDefinitionTemplate30) gribFile.getGridDefinitionTemplate();
//		System.out.println( "Covered area:" );
//		System.out.println( "   from (latitude, longitude): " + GribFile.unitsToDeg( gridDefinition.firstPointLat ) + ", " + GribFile
//				.unitsToDeg( gridDefinition.firstPointLon ) );
//		System.out.println( "   to: (latitude, longitude): " + GribFile.unitsToDeg( gridDefinition.lastPointLat ) + ", " + GribFile
//				.unitsToDeg( gridDefinition.lastPointLon ) );
//
//		// Get grid data
//		double latitude = 23.33;
//		double longitude = 18.45;
//		System.out.println( "Value at (" + latitude + ", " + longitude + "): " + gribFile.interpolateValueAtLocation( gridid, latitude, longitude ) );
//	}

	//	@Test
	public void generateMapName() throws WindResourceFileNotFound {
		// Given
		final GeoLocation location = new GeoLocation.Builder()
				.withLatitude( 48.5 )
				.withLongitude( 2.1 )
				.build();
		final WindMapHandler mapHandler = new WindMapHandler.Builder().build();
		// Test
		final String obtained = mapHandler.generateMapName( location );
		// Assertions
		Assertions.assertEquals( "", obtained );
	}

//	@Test
	public void readGribData() throws WindResourceFileNotFound {
		// Given
		final WindMapHandler mapHandler = new WindMapHandler.Builder()
//				.withGRIB2DataFile( TEST_GRIB_DATA_DIRECTORY + TEST_GRIB_DATA_FILE )
				.build();
		// Test
//		mapHandler
		// Assertions
		Assertions.assertEquals( 1, mapHandler.getWindMapsCount() );
		Assertions.assertEquals( 9, mapHandler.getWindCellsOnMap( TEST_MAP_DATA_DATE_REFERENCE ) );
	}

//	@Test
	public void windData4Location() throws WindResourceFileNotFound, WindResourceNotFound {
		// Given
		final GeoLocation location = new GeoLocation.Builder().withLatitude( TEST_LATITUDE ).withLongitude( TEST_LONGITUDE ).build();
		final WindMapHandler mapHandler = new WindMapHandler.Builder()
//				.withGRIB2DataFile( TEST_GRIB_DATA_DIRECTORY + TEST_GRIB_DATA_FILE )
				.build();
		// Assertions
		Assertions.assertEquals( 10.2, mapHandler.windData4Location( location, Optional.empty() ).getWindSpeed() );
		Assertions.assertEquals( 42, mapHandler.windData4Location( location, Optional.empty() ).getWindDirection() );
	}

//	@Test
	public void windData4LocationFailure() throws WindResourceFileNotFound, WindResourceNotFound {
		// Given
		final GeoLocation location = new GeoLocation.Builder().withLatitude( TEST_LATITUDE_OUTOFMAP ).withLongitude( TEST_LONGITUDE_OUTOFMAP )
				.build();
		final WindMapHandler mapHandler = new WindMapHandler.Builder()
//				.withGRIB2DataFile( TEST_GRIB_DATA_DIRECTORY + TEST_GRIB_DATA_FILE )
				.build();
		// Exceptions
		Assertions.assertThrows( WindResourceNotFound.class, () -> {
			mapHandler.windData4Location( location, Optional.empty() );
		} );
	}
}

package org.dimensinfin.virtualregatta.routecalculator.support;

import org.dimensinfin.virtualregatta.routecalculator.io.parser.output.OutputManager;

public class LogOutputManager extends OutputManager {
	private LogOutputManager() {}

	// - B U I L D E R
	public static class Builder {
		private LogOutputManager onConstruction;

		public Builder() {
			this.onConstruction = new LogOutputManager();
		}

		public LogOutputManager build() {
			return this.onConstruction;
		}
	}
}

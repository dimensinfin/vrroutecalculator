package org.dimensinfin.virtualregatta.routecalculator.core.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

public class VMCDataTest {
	private static final double DELTA_ERROR = 0.000001;

	@Test
	public void buildComplete() {
		final WindData windData = Mockito.mock( WindData.class );
		final VMCData vmcData = new VMCData.Builder()
				.withWindData( windData )
				.withPolars( new Polars() )
				.build();
		Assertions.assertNotNull( vmcData );
	}

	@Test
	public void getSpeed() {
		// Given
		final WindData windData = new WindData()
				.setLongitude( 45.0 )
				.setLatitude( 60.0 )
				.setSize( 0.5 )
				.setWindDirection( 0 )
				.setWindSpeed( 12.0 );
		final VMCData vmcData = new VMCData.Builder()
				.withWindData( windData )
				.withPolars( new Polars() )
				.build();
		// Test
		final double obtained = vmcData.getSpeed( 45 );
		// Assertions
		Assertions.assertEquals( 10.33, obtained, DELTA_ERROR );
	}
}

package org.dimensinfin.virtualregatta.routecalculator.router;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.LocationNotInMap;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.TimedRoute;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

public class TimedRouteGeneratorTest {
	@Test
	public void buildComplete() {
		final WindMapHandler windMapHandler = Mockito.mock( WindMapHandler.class );
		final TimedRouteGenerator timedRouteGenerator = new TimedRouteGenerator.Builder()
				.withBoatPolars( new Polars() )
				.withIsochroneSeparation( 15 )
				.withWindMapHandler( windMapHandler )
				.build();
		Assertions.assertNotNull( timedRouteGenerator );
	}

	@Test
	public void buildFailure() {
		final WindMapHandler windMapHandler = Mockito.mock( WindMapHandler.class );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRouteGenerator.Builder()
					.withBoatPolars( null )
					.withIsochroneSeparation( 15 )
					.withWindMapHandler( windMapHandler )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRouteGenerator.Builder()
					.withBoatPolars( new Polars() )
					.withIsochroneSeparation( 15 )
					.withWindMapHandler( null )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRouteGenerator.Builder()
					.withIsochroneSeparation( 15 )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRouteGenerator.Builder()
					.withIsochroneSeparation( null )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRouteGenerator.Builder()
					.withBoatPolars( new Polars() )
					.withIsochroneSeparation( 15 )
					.build();
		} );
	}

	@Test
	public void newRoute() throws WindResourceNotFound, LocationNotInMap {
		// Given
		final WindMapHandler windMapHandler = Mockito.mock( WindMapHandler.class );
		final GeoLocation start = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( -4.0 )
				.build();
		final double direction = -90.0;
		final WindData windData = new WindData()
				.setLatitude( 45.0 )
				.setLongitude( -4.0 )
				.setSize( 0.5 )
				.setWindDirection( 0 )
				.setWindSpeed( 12.0 );
		// When
		Mockito.when( windMapHandler.windData4Location( Mockito.any( GeoLocation.class ), Mockito.any( Optional.class ) ) )
				.thenReturn( windData );
		// Test
		final TimedRouteGenerator timedRouteGenerator = new TimedRouteGenerator.Builder()
				.withBoatPolars( new Polars() )
				.withIsochroneSeparation( 15 )
				.withWindMapHandler( windMapHandler )
				.build();
		final TimedRoute obtained = (TimedRoute) timedRouteGenerator.newRoute( start, direction );
		// Assertions
		Assertions.assertNotNull( obtained );
		Assertions.assertEquals( 7, obtained.getRouteLocations().size() );
	}

	@Test
	public void newRouteFailure() throws WindResourceNotFound {
		// Given
		final WindMapHandler windMapHandler = Mockito.mock( WindMapHandler.class );
		final GeoLocation start = Mockito.mock( GeoLocation.class );
		// When
		Mockito.when( windMapHandler.windData4Location( Mockito.any( GeoLocation.class ), Mockito.any( Optional.class ) ) )
				.thenThrow(  WindResourceNotFound.class );
		// Exceptions
		Assertions.assertThrows( WindResourceNotFound.class, () -> {
			new TimedRouteGenerator.Builder()
					.withBoatPolars( new Polars() )
					.withIsochroneSeparation( 15 )
					.withWindMapHandler( windMapHandler )
					.build()
					.newRoute( start, 90.0 );
		} );
	}
}

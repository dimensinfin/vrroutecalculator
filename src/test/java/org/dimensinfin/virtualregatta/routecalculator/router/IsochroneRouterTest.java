package org.dimensinfin.virtualregatta.routecalculator.router;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.LocationNotInMap;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;
import org.dimensinfin.virtualregatta.routecalculator.io.parser.output.OutputManager;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

import static org.dimensinfin.virtualregatta.routecalculator.router.IsochroneRouter.DEFAULT_ISOCRONE_SEPARATION_MINUTES;

public class IsochroneRouterTest {

	private OutputManager outputManager;
	private RouterConfiguration routerConfiguration;
	private WindMapHandler windMapHandler;

	@BeforeEach
	public void beforeEach() {
		this.outputManager = Mockito.mock( OutputManager.class );
		this.routerConfiguration = Mockito.mock( RouterConfiguration.class );
		this.windMapHandler = Mockito.mock( WindMapHandler.class );
	}

	@Test
	public void buildComplete() {
		final IsochroneRouter router = new IsochroneRouter.Builder()
				.withOutputManager( this.outputManager )
				.withRouterConfiguration( this.routerConfiguration )
				.withIsochronePeriod( 30 )
				.withWindMapHandler( this.windMapHandler )
				.withPolars( new Polars() )
				.build();
		Assertions.assertNotNull( router );
		Assertions.assertEquals( 30, router.isochronePeriod );
	}

	@Test
	public void buildDefault() {
		final IsochroneRouter router = new IsochroneRouter.Builder()
				.withOutputManager( this.outputManager )
				.withRouterConfiguration( this.routerConfiguration )
				.withWindMapHandler( this.windMapHandler )
				.build();
		Assertions.assertNotNull( router );
		Assertions.assertEquals( DEFAULT_ISOCRONE_SEPARATION_MINUTES, router.isochronePeriod );
	}

	@Test
	public void buildFailure() {
		Assertions.assertThrows( NullPointerException.class, () -> {
			new IsochroneRouter.Builder()
					.withOutputManager( null )
					.withRouterConfiguration( this.routerConfiguration )
					.withIsochronePeriod( 30 )
					.withWindMapHandler( this.windMapHandler )
					.withPolars( new Polars() )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new IsochroneRouter.Builder()
					.withOutputManager( this.outputManager )
					.withRouterConfiguration( null )
					.withIsochronePeriod( 30 )
					.withWindMapHandler( this.windMapHandler )
					.withPolars( new Polars() )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new IsochroneRouter.Builder()
					.withOutputManager( this.outputManager )
					.withRouterConfiguration( this.routerConfiguration )
					.withIsochronePeriod( 30 )
					.withWindMapHandler( null )
					.withPolars( new Polars() )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new IsochroneRouter.Builder()
					.withRouterConfiguration( this.routerConfiguration )
					.withIsochronePeriod( 30 )
					.withWindMapHandler( this.windMapHandler )
					.withPolars( new Polars() )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new IsochroneRouter.Builder()
					.withOutputManager( this.outputManager )
					.withIsochronePeriod( 30 )
					.withWindMapHandler( this.windMapHandler )
					.withPolars( new Polars() )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new IsochroneRouter.Builder()
					.withOutputManager( this.outputManager )
					.withRouterConfiguration( this.routerConfiguration )
					.withIsochronePeriod( 30 )
					.withPolars( new Polars() )
					.build();
		} );
	}

	@Test
	public void generateRouteFan() throws WindResourceNotFound, LocationNotInMap {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder() // Golfo de Vizcaya
				.withLatitude( 45.0 )
				.withLongitude( -4.0 )
				.build();
		final GeoLocation waypoint = new GeoLocation.Builder() // Golfo de Vizcaya
				.withLatitude( 45.0 )
				.withLongitude( -6.0 )
				.build();
		final WindData windData = new WindData()
				.setLatitude( 60.0 )
				.setLongitude( 45.0 )
				.setSize( 0.5 )
				.setWindDirection( 0 )
				.setWindSpeed( 12.0 );
		// When
		Mockito.when( this.routerConfiguration.getLeftDeviationAngle() ).thenReturn( -1 );
		Mockito.when( this.routerConfiguration.getRightDeviationAngle() ).thenReturn( 1 );
		Mockito.when( this.windMapHandler.windData4Location( Mockito.any( GeoLocation.class ), Mockito.any( Optional.class ) ) )
				.thenReturn( windData );
		// Test
		final IsochroneRouter router = new IsochroneRouter.Builder()
				.withOutputManager( this.outputManager )
				.withRouterConfiguration( this.routerConfiguration )
				.withWindMapHandler( this.windMapHandler )
				.withStartLocation( startLocation )
				.withWaypoint( waypoint )
				.build();
		final List<Route> obtained = router.generateRouteSet();
		// Assertions
		Assertions.assertNotNull( obtained );
		Assertions.assertTrue( obtained.size() > 0 );
		Assertions.assertTrue( obtained.size() == 3 );
	}

	@Test
	public void reportRoute() throws IOException, WindResourceNotFound {
		// Given
		final OutputManager testOutputManager = new OutputManager.Builder()
				.withDestination( new BufferedWriter( new FileWriter( "./src/test/resources/output/RouteDump.txt", false ) ) )
				.build();
		final GeoLocation startLocation = new GeoLocation.Builder() // Golfo de Vizcaya
				.withLatitude( 45.0 )
				.withLongitude( -4.0 )
				.build();
		final GeoLocation waypoint = new GeoLocation.Builder() // Golfo de Vizcaya
				.withLatitude( 45.0 )
				.withLongitude( -6.0 )
				.build();
		final WindData windData = new WindData()
				.setLatitude( 60.0 )
				.setLongitude( 45.0 )
				.setSize( 0.5 )
				.setWindDirection( 0 )
				.setWindSpeed( 12.0 );
		// When
		Mockito.when( this.routerConfiguration.getLeftDeviationAngle() ).thenReturn( -1 );
		Mockito.when( this.routerConfiguration.getRightDeviationAngle() ).thenReturn( 1 );
		Mockito.when( this.windMapHandler.windData4Location( Mockito.any( GeoLocation.class ), Mockito.any( Optional.class ) ) )
				.thenReturn( windData );
		// Test
		final IsochroneRouter router = new IsochroneRouter.Builder()
				.withOutputManager( testOutputManager )
				.withRouterConfiguration( this.routerConfiguration )
				.withWindMapHandler( this.windMapHandler )
				.withStartLocation( startLocation )
				.withWaypoint( waypoint )
				.build();
		final List<Route> routeList = router.generateRouteSet();
		router.reportRoute( "TestRoute", routeList.get( 0 ), startLocation, waypoint );
		// Assertions
		Assertions.assertNotNull( routeList );
	}
}

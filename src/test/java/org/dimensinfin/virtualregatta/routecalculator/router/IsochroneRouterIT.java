package org.dimensinfin.virtualregatta.routecalculator.router;

import org.dimensinfin.virtualregatta.routecalculator.io.parser.output.OutputManager;
import org.dimensinfin.virtualregatta.routecalculator.support.LogOutputManager;

public class IsochroneRouterIT {
	public void validateNorthRoute() {
		// Given
		final OutputManager outputManager = new LogOutputManager.Builder().build();
		final RouterConfiguration routerConfiguration = new RouterConfiguration.Builder().build();
		final RouteGenerator routeGeneratorTimedRoutes = new TimedRouteGenerator.Builder()
				.withIsochroneSeparation( 15 ).build();
		final IsochroneRouter router = new IsochroneRouter.Builder()
				.withOutputManager( outputManager )
				.withRouterConfiguration( routerConfiguration )
//				.withRouteGenerator( routeGeneratorTimedRoutes )
				.build();

	}
}

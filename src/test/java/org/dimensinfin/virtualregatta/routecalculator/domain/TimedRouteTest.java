package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.LocationNotInMap;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.router.RouteGenerator;
import org.dimensinfin.virtualregatta.routecalculator.router.RouterType;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

public class TimedRouteTest {
	private static final double DELTA_ERROR = 0.000001;
	private static final Integer TEST_BOAT_DIRECTION = 15;
	private static final Integer TEST_ISOCHRONE_PERIOD = 15;
	private static final String TEST_ROUTE_NAME = "-TEST_ROUTE_NAME-";
	private static final double TEST_SPEED = 6.9;
	private static final int TEST_TIME = 15;
	private Polars polars;
	private GeoLocation location;
	private WindMapHandler windMapHandler;
	private RouteGenerator routeGenerator;

	@Test
	public void accessWindCellSearchTime() {
		// Given
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( this.polars )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( this.location )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
//		final WindCell windCell = Mockito.mock( WindCell.class );
		final Instant testStartTime = Instant.now();
		route.startRoute();
		// Assertions
		Assertions.assertTrue( (testStartTime.toEpochMilli() - route.startTime.toEpochMilli()) < 100 );
		Assertions.assertEquals( Duration.ZERO, route.elapsedRouteTime );
		Assertions.assertEquals( TEST_BOAT_DIRECTION, route.getDirection() );
		route.startTime = testStartTime;
		route.elapsedRouteTime = Duration.of( 32, ChronoUnit.MINUTES );
		final ZonedDateTime expected = testStartTime.plus( Duration.ofMinutes( 32 ) ).atZone( ZoneId.of( "GMT" ) );
		final ZonedDateTime obtained = route.accessWindCellSearchTime();
		Assertions.assertEquals( expected, obtained );
	}

	@BeforeEach
	public void beforeEach() {
		this.polars = Mockito.mock( Polars.class );
		this.location = Mockito.mock( GeoLocation.class );
		this.routeGenerator = Mockito.mock( RouteGenerator.class );
		this.windMapHandler = Mockito.mock( WindMapHandler.class );
	}

	@Test
	public void buildComplete() {
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( this.polars )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( this.location )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		Assertions.assertNotNull( route );
	}

	@Test
	public void buildFailureMissing() {
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withPolars( this.polars )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
					.withWindMapHandler( this.windMapHandler )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.build();
		} );
	}

	@Test
	public void buildFailureNull() {
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( null )
					.withPolars( this.polars )
//					.withGenerator( this.routeGenerator )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( null )
//					.withGenerator( this.routeGenerator )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
//					.withGenerator( null )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
//					.withGenerator( this.routeGenerator )
					.withWindMapHandler( null )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
//					.withGenerator( this.routeGenerator )
					.withWindMapHandler( this.windMapHandler )
					.withStart( null )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
//					.withGenerator( this.routeGenerator )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( null )
					.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new TimedRoute.Builder()
					.withName( TEST_ROUTE_NAME )
					.withPolars( this.polars )
//					.withGenerator( this.routeGenerator )
					.withWindMapHandler( this.windMapHandler )
					.withStart( this.location )
					.withDirection( TEST_BOAT_DIRECTION )
					.withIsochronePeriod( null )
					.build();
		} );
	}

	@Test
	public void calculateNextIsochronePoint() throws WindResourceNotFound, LocationNotInMap {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 60.0 )
				.withLongitude( 45.0 )
				.build();
		final int direction = -45;
		final int time = 15;
		final WindData windData = new WindData()
				.setLatitude( 60.0 )
				.setLongitude( 45.0 )
				.setSize( 0.5 )
				.setWindDirection( 0 )
				.setWindSpeed( 12.0 );
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		// When
		Mockito.when( this.windMapHandler.windData4Location( startLocation, Optional.empty() ) ).thenReturn( windData );
		// Assertions
		Assertions.assertEquals( "59 58' N", route.calculateNextIsochronePoint( startLocation, direction, time ).formatLatitude() );
		Assertions.assertEquals( "045 02' E", route.calculateNextIsochronePoint( startLocation, direction, time ).formatLongitude() );
	}

	@Test
	public void evaluateEndCondition() {
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( this.polars )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( this.location )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		// Assertions
		Assertions.assertFalse( route.evaluateEndCondition() );
		route.elapsedRouteTime = route.elapsedRouteTime.plus( Duration.ofMinutes( 15 ) );
		Assertions.assertFalse( route.evaluateEndCondition() );
		route.elapsedRouteTime = route.elapsedRouteTime.plus( Duration.ofMinutes( 90 ) );
		Assertions.assertTrue( route.evaluateEndCondition() );
	}

	@Test
	public void generateRoute() throws LocationNotInMap, WindResourceNotFound {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 60.0 )
				.withLongitude( 45.0 )
				.build();
		final WindData windData = new WindData()
				.setLatitude( 60.0 )
				.setLongitude( 45.0 )
				.setSize( 0.5 )
				.setWindDirection( 0 )
				.setWindSpeed( 12.0 );
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		// When
		Mockito.when( this.windMapHandler.windData4Location( Mockito.any( GeoLocation.class ), Mockito.any( Optional.class ) ) )
				.thenReturn( windData );
		// Test
		final Route obtained = route.generateRoute();
		// Assertions
		Assertions.assertNotNull( obtained );
	}

	@Test
	public void getterContract() {
		// Given
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( this.polars )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( this.location )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		// When
		Mockito.when( this.routeGenerator.getType() ).thenReturn( RouterType.ISO );
		// Assertions
		Assertions.assertEquals( RouteAlgorithmType.TIMED, route.getType() );
		Assertions.assertEquals( TEST_ROUTE_NAME, route.getName() );
//		Assertions.assertEquals( RouterType.ISO, route.getGenerator().getType() );
		Assertions.assertEquals( TEST_BOAT_DIRECTION, route.getDirection() );
		Assertions.assertEquals( TEST_ISOCHRONE_PERIOD, route.getIsochronePeriod() );
	}

	@Test
	public void run4TimeE() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = 90;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "45 00' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "002 03' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeN() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = 0;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "44 58' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "002 00' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeNE() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = 45;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "44 59' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "002 02' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeNW() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = -45;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "44 59' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "001 58' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeS() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = -180;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "45 02' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "002 00' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeSE() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = 90 + 45;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "45 01' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "002 02' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeSW() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = -90 - 45;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "45 01' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "001 58' E", endLocation.formatLongitude() );
	}

	@Test
	public void run4TimeW() {
		// Given
		final GeoLocation startLocation = new GeoLocation.Builder()
				.withLatitude( 45.0 )
				.withLongitude( 2.0 )
				.build();
		final double speed = TEST_SPEED;
		final int direction = -90;
		final int time = TEST_TIME;
		// Test
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( new Polars() )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( startLocation )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		route.startRoute();
		final GeoLocation endLocation = route.run4Time( startLocation, speed, direction, time );
		// Assertions
		Assertions.assertEquals( "45 00' N", endLocation.formatLatitude() );
		Assertions.assertEquals( "001 57' E", endLocation.formatLongitude() );
	}

	@Test
	public void setterContract() {
		// Given
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( this.polars )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( this.location )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		// Assertions
		Assertions.assertEquals( TEST_ROUTE_NAME, route.getName() );
		route.setName( "-TEST_NEW_ROUTENAME-" );
		Assertions.assertEquals( "-TEST_NEW_ROUTENAME-", route.getName() );
	}

	@Test
	public void startRoute() {
		// Given
		final TimedRoute route = new TimedRoute.Builder()
				.withName( TEST_ROUTE_NAME )
				.withPolars( this.polars )
//				.withGenerator( this.routeGenerator )
				.withWindMapHandler( this.windMapHandler )
				.withStart( this.location )
				.withDirection( TEST_BOAT_DIRECTION )
				.withIsochronePeriod( TEST_ISOCHRONE_PERIOD )
				.build();
		final Instant testStartTime = Instant.now();
		// Test
		route.startRoute();
		// Assertions
		Assertions.assertTrue( (testStartTime.toEpochMilli() - route.startTime.toEpochMilli()) < 100 );
		Assertions.assertEquals( Duration.ZERO, route.elapsedRouteTime );
	}
}

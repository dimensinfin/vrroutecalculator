package org.dimensinfin.virtualregatta.routecalculator.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GeoLocationTest {
	private static final Double TEST_START_LONGITUDE = 2.84;
	private static final Double TEST_START_LATITUDE = 45.35;

	@Test
	public void angleTo() {
		// Given
		final GeoLocation geoLocation = new GeoLocation.Builder()
				.withLongitude( TEST_START_LONGITUDE )
				.withLatitude( TEST_START_LATITUDE )
				.build();
		final GeoLocation destinationAtFront = new GeoLocation.Builder()
				.withLongitude( TEST_START_LONGITUDE )
				.withLatitude( TEST_START_LATITUDE - 1.0 )
				.build();
		// Assertions
		Assertions.assertEquals( 0.0, geoLocation.angleTo( destinationAtFront ) );
	}

	@Test
	public void courseAngle() {
		// Given
		final GeoLocation geoLocation = new GeoLocation.Builder()
				.withLongitude( TEST_START_LONGITUDE )
				.withLatitude( TEST_START_LATITUDE )
				.build();
		final GeoLocation destinationAtFront = new GeoLocation.Builder()
				.withLongitude( TEST_START_LONGITUDE )
				.withLatitude( TEST_START_LATITUDE - 1.0 )
				.build();
		// Assertions
		Assertions.assertEquals( 0.0, geoLocation.courseAngle( destinationAtFront ) );
	}

	@Test
	public void buildComplete() {
		final GeoLocation geoLocation = new GeoLocation.Builder()
				.withLongitude( TEST_START_LONGITUDE )
				.withLatitude( TEST_START_LATITUDE )
				.build();
		Assertions.assertNotNull( geoLocation );
	}

	@Test
	public void buildFailure() {
		Assertions.assertThrows( NullPointerException.class, () -> {
			new GeoLocation.Builder()
					.withLongitude( null )
					.withLatitude( TEST_START_LATITUDE )
					.build();
		} );
		Assertions.assertThrows( NullPointerException.class, () -> {
			new GeoLocation.Builder()
					.withLongitude( TEST_START_LONGITUDE )
					.withLatitude( null )
					.build();
		} );
	}
}

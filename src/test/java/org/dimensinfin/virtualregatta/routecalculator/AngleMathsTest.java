package org.dimensinfin.virtualregatta.routecalculator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.dimensinfin.virtualregatta.routecalculator.math.AngleMaths;

public class AngleMathsTest {
	@Test
	public void angleDifference() {
		Assertions.assertEquals( 32, new AngleMaths().angleDifference( 0, 32 ) );
	}
}

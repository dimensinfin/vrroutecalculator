package org.dimensinfin.virtualregatta.routecalculator.math;

public enum LengthUnit {
	KM {
		public Double toLatitude( final Double length ) { return length / LengthUnit.KM.KM_LATITUDE_CONSTANT(); }

		public Double toLongitude( final Double length, final Double latitudeRads ) {
			return length / (LengthUnit.KM.KM_LONTITUDE_CONSTANT() * Math.cos( latitudeRads ));
		}
	};

	public Double KM_LATITUDE_CONSTANT() {
		return 110.574;
	}

	public Double KM_LONTITUDE_CONSTANT() {
		return 111.320;
	}

	public Double toLatitude( final Double length ) { throw new AbstractMethodError(); }

	public Double toLongitude( final Double length, final Double latitudeRads ) { throw new AbstractMethodError(); }
}

package org.dimensinfin.virtualregatta.routecalculator.math;

public enum RealTimeUnit {
	MINUTES {
		public Double toSeconds( Double time ) { return time * 60.0; }

		public Double toHours( Double time ) { return time / 60.0; }
	};

	public Double toHours( final Double time ) {
		throw new AbstractMethodError();
	}

	public Double toSeconds( final Double time ) {
		throw new AbstractMethodError();
	}
}

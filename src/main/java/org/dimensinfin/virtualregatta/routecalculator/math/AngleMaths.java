package org.dimensinfin.virtualregatta.routecalculator.math;

import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;

public class AngleMaths {
	private static final AngleMaths singleton = new AngleMaths.Builder().build();

	public static AngleMaths singleton() {
		return singleton;
	}

	@Deprecated
	public AngleMaths() {}

	public  double normalizeAngle( final double heading ) {
		return this.adjustAngleTo360( heading );
	}
	/**
	 * Reduces the parameter angle to be always between 0 and 360. Negative angles are complemented.
	 * This version accepts angles in double precision.
	 *
	 * @param heading the input heading angle
	 * @return the equivalent angle but between 0 and 360 in the positive values.
	 */
	public  double adjustAngleTo360( final double heading ) {
		if (heading == 0.0) return 360.0;
		if (heading > 360.0) return GeoLocation.adjustAngleTo360( heading - 360.0 );
		if (heading < 0.0) return GeoLocation.adjustAngleTo360( heading + 360.0 );
		return heading;
	}

	/**
	 * Reduces the parameter angle to be always between 0 and 360. Negative angles are complemented.
	 * This version accepts angles in integer precision.
	 *
	 * @param heading the input heading angle
	 * @return the equivalent angle but between 0 and 360 in the positive values.
	 */
	public  int adjustAngleTo360( final int heading ) {
		if (heading == 0) return 360;
		if (heading > 360) return GeoLocation.adjustAngleTo360( heading - 360 );
		if (heading < 0) return GeoLocation.adjustAngleTo360( heading + 360 );
		return heading;
	}
	/**
	 * Calculate the difference in grades between two angles taking on account the 0-360 rose wind.
	 *
	 * @param alpha1 angle 1 to compare
	 * @param alpha2 angle 2 to compare
	 * @return the difference in degrees between angle 1 towards angle 2.
	 */
	public double angleDifference( final double alpha1, final double alpha2 ) {
		final double dalpha = Math.toRadians( alpha1 ) - Math.toRadians( alpha2 );
		final double continuous_alpha = Math.acos( Math.cos( dalpha ) );

		//		double a1 = GeoLocation.adjustAngleTo360(angle1);
		//		double a2 = GeoLocation.adjustAngleTo360(angle2);
		//		if(a1>180.0)a1=360.0-a1;
		//		if(a2>180.0)a2=360.0-a2;
		return GeoLocation.adjustAngleTo360( Math.toDegrees( continuous_alpha ) );
	}

	public int angleDifference( final int angle1, final int angle2 ) {
		return new Double( GeoLocation.angleDifference( new Double( angle1 ), new Double( angle2 ) ) ).intValue();
	}

	/**
	 * Calculate the AWd between the course of the boat and the wind direction. We have to take care of the
	 * limit when the boat angle and the wind are at different sides of the 0-360 course.
	 *
	 * @param windAngle the wind direction. Usually this angle points opposite to the movement angle
	 * @param boatAngle the angle of the boat movement. Apparent Wind Direction.
	 */
	public int calculateAWD( final double windAngle, final double boatAngle ) {
		double angle = windAngle - boatAngle;
		if (angle < 0.0) {
			if (angle < -180.0) angle += 360.0;
		} else if (angle > 180.0) angle -= 360.0;
		return (int) Math.round( angle );
	}

	// - B U I L D E R
	public static class Builder {
		private AngleMaths onConstruction;

		public Builder() {
			this.onConstruction = new AngleMaths();
		}

		public AngleMaths build() {
			return this.onConstruction;
		}
	}
}

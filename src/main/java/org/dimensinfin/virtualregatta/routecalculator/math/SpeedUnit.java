package org.dimensinfin.virtualregatta.routecalculator.math;

public enum SpeedUnit {
	KMH {
		public Double toKNOTS( Double speed ) { return speed / SpeedUnit.KMH.KNOTS_KM_CONVERSION_CONSTANT() / 1000.0; }

		public Double toKMH( Double m ) { return m; }

		public Double toKMS( Double m ) { return m / 3600.0; }

		public Double toCMS( Double m ) { return m * 1000.0 * 100.0 / 3600.0; }
	},
	KMS {
		public Double toKNOTS( Double speed ) { return speed / SpeedUnit.KMH.KNOTS_KM_CONVERSION_CONSTANT() / 3600.0; }

		public Double toKMH( Double m ) { return m * 3600.0; }

		public Double toKMS( Double m ) { return m; }

		public Double toCMS( Double m ) { return m * 1000.0 * 100.0; }
	},
	CMS {
		public Double toKNOTS( Double speed ) { return speed / SpeedUnit.KMH.KNOTS_KM_CONVERSION_CONSTANT() / 1000.0 / 100.0 / 3600.0; }

		public Double toKMH( Double m ) { return m / 1000.0 / 100.0 / 3600.0; }

		public Double toKMS( Double m ) { return m / 1000.0 / 100.0; }

		public Double toCMS( Double m ) { return m; }
	},
	KNOTS {
		public Double toKNOTS( Double speed ) { return speed; }

		public Double toKMH( Double speed ) { return speed * SpeedUnit.KMH.KNOTS_KM_CONVERSION_CONSTANT()/1000.0; }

		public Double toKMS( Double speed ) { return speed * SpeedUnit.KMH.KNOTS_KM_CONVERSION_CONSTANT()/1000.0 / 3600.0; }

		public Double toCMS( Double speed ) { return speed * SpeedUnit.KMH.KNOTS_KM_CONVERSION_CONSTANT()/1000.0 / 3600.0 * 1000.0 * 100.0; }
	};

	public Double toCMS( final Double speed ) {
		throw new AbstractMethodError();
	}

	public Double toKMH( final Double speed ) {
		throw new AbstractMethodError();
	}

	public Double toKMS( final Double speed ) {
		throw new AbstractMethodError();
	}

	public Double toKNOTS( final Double speed ) {
		throw new AbstractMethodError();
	}

	private Integer KNOTS_KM_CONVERSION_CONSTANT() {
		return 1852;
	}
}

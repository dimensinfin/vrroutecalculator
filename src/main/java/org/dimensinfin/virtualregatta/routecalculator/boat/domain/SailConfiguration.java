package org.dimensinfin.virtualregatta.routecalculator.boat.domain;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

/**
 * Defines a sail configuration and a sail speed.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class SailConfiguration {
	protected Double speed = 0.0;
	protected Sails sail = Sails.JIB;

	@Deprecated
	public SailConfiguration() {
	}

	public SailConfiguration( final Sails sail, final double newSpeed ) {
		this.sail = sail;
		speed = newSpeed;
	}

	public Sails getSail() {
		return sail;
	}

	public void setSail( final Sails sail ) {
		this.sail = sail;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed( final double speed ) {
		this.speed = speed;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "[SailConfiguration " );
		NumberFormat nf = NumberFormat.getInstance( Locale.ENGLISH );
		nf.setMaximumFractionDigits( 3 );
		nf.setMinimumFractionDigits( 3 );
		buffer.append( sail ).append( " - " );
		buffer.append( "speed=" ).append( nf.format( speed ) ).append( "]" );
		return buffer.toString();
	}

	// - B U I L D E R
	public static class Builder {
		private SailConfiguration onConstruction;

		public Builder() {
			this.onConstruction = new SailConfiguration();
		}

		public SailConfiguration build() {
			Objects.requireNonNull( this.onConstruction.speed );
			Objects.requireNonNull( this.onConstruction.sail );
			return this.onConstruction;
		}

		public SailConfiguration.Builder withSailType( final Sails sail ) {
			this.onConstruction.sail = sail;
			return this;
		}

		public SailConfiguration.Builder withSpeed( final Double speed ) {
			this.onConstruction.speed = Objects.requireNonNull( speed );
			return this;
		}
	}
}

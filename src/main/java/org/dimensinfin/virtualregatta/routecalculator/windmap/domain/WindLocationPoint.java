package org.dimensinfin.virtualregatta.routecalculator.windmap.domain;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class WindLocationPoint {
	@JsonProperty("latitudeDegree")
	private Integer latitudeDegree;
	@JsonProperty("latitudeMinutes")
	private Integer latitudeMinutes;
	@JsonProperty("longitudeDegree")
	private Integer longitudeDegree;
	@JsonProperty("longitudeMinutes")
	private Integer longitudeMinutes;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	@JsonProperty("latitudeDegree")
	public Integer getLatitudeDegree() {
		return latitudeDegree;
	}
	@JsonProperty("latitudeDegree")
	public void setLatitudeDegree(Integer latitudeDegree) {
		this.latitudeDegree = latitudeDegree;
	}

	@JsonProperty("latitudeMinutes")
	public Integer getLatitudeMinutes() {
		return latitudeMinutes;
	}

	@JsonProperty("latitudeMinutes")
	public void setLatitudeMinutes(Integer latitudeMinutes) {
		this.latitudeMinutes = latitudeMinutes;
	}

	@JsonProperty("longitudeDegree")
	public Integer getLongitudeDegree() {
		return longitudeDegree;
	}

	@JsonProperty("longitudeDegree")
	public void setLongitudeDegree(Integer longitudeDegree) {
		this.longitudeDegree = longitudeDegree;
	}

	@JsonProperty("longitudeMinutes")
	public Integer getLongitudeMinutes() {
		return longitudeMinutes;
	}

	@JsonProperty("longitudeMinutes")
	public void setLongitudeMinutes(Integer longitudeMinutes) {
		this.longitudeMinutes = longitudeMinutes;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}

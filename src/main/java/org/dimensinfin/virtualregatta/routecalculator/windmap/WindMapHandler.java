package org.dimensinfin.virtualregatta.routecalculator.windmap;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceFileNotFound;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

/**
 * It is possible to acces the game map weather data. There are URLs that will return that weather information in XML document format. The map
 * information returned covers a big cell of 1 degree in latitude and 1 degree in longitude.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class WindMapHandler {
	public static final String WIND_DATA_DATE_FORMAT = "yyyy-MM-dd HH";
	private static final int numskip = 0;
	private static final int gridid = 0;
	private final String protocol = "http";
	private final String host = "volvogame.virtualregatta.com";
	private final String prefix = "/resources/winds/meteo_";
	private String timeReference; // TODO - Stores the fixed date reference for the weather data
	private Map<String, List<WindData>> windMaps = new HashMap<>(); // Stores the wind maps for the different prevision hours.
	private String gribFileName;
//	private RandomAccessGribFile gribFile;

	public int getWindMapsCount() {
		return windMaps.size();
	}

//	/**
//	 * Gets the wind cell that corresponds to the target location and to the target time.
//	 *
//	 * @param target     the geo location to check is falls inside the cell area
//	 * @param selectTime the point in time where this expected weather information is available. A single cell can have different weather data
//	 *                   depending on the time of day.
//	 * @return the matching wind cell or an exception of the search fails.
//	 */
//	public WindCell cellWithPoint( final GeoLocation target, final ZonedDateTime selectTime ) throws LocationNotInMap, WindResourceNotFound {
////		if (null == WindMapHandler.windMaps) {
////			// - The map is empty so load the first point.
//////			WindMapHandler.windMaps = new HashMap<>();
////			WindMapHandler.loadWinds( target );
////		}
////
////		//- Check if the wind maps need a reload. This test is only done at the wind change times.
////		if (WindMapHandler.needReload()) {
////			WindMapHandler.clear();
////			return WindMapHandler.cellWithPoint( target, selectTime );
////		}
////		final WindMap activeMap = WindMapHandler.getActiveMap( selectTime );
////		// - If the map is null it can be because of error or that the cell data is not loaded.
////		if (null != activeMap)
////			try {
////				return activeMap.cellWithPoint( target );
////			} catch (final LocationNotInMap lnime) {
////				WindMapHandler.loadWinds( target );
////				return activeMap.cellWithPoint( target );
////			}
////		else
////			throw new LocationNotInMap( "The " + target.toString() + " location is not contained inside the Wind Map." );
//		throw new WindResourceNotFound( target, "Undefined" );
//	}

	public int getWindCellsOnMap( final String mapDateReference ) {
		if (this.windMaps.containsKey( mapDateReference )) return this.windMaps.get( mapDateReference ).size();
		else return 0;
	}

	public WindData windData4Location( final GeoLocation targetLocation, final Optional<ZonedDateTime> zonedDateTime ) throws WindResourceNotFound {
		// TODO - Now the date timed weather data is blocked. Use the predefined identifier.
		final List<WindData> windMap = this.windMaps.get( this.timeReference );
		// Search the list for a cell that contains the location.
		for (WindData data : windMap)
			if (data.contains( targetLocation )) return data;
		throw new WindResourceNotFound( targetLocation, "Coordinates do not map to any wind cell available." );
	}

	protected String generateMapName( final GeoLocation location ) {
		// - Calculate the wind box that matches this location.
		final double latitude = Math.floor( location.getLat() + 0.5 );
		final double l2 = Math.round( latitude / 10.0 ) * 10.0;
		int mapLat;
		if (latitude > l2)
			mapLat = new Double( l2 ).intValue() + 10;
		else
			mapLat = new Double( l2 ).intValue();

		// - Check if the longitude is below or after the 0.5 limit.
		final double minutes = location.getLon() - Math.floor( location.getLon() );
		long longitude;
		if (minutes >= 0.5)
			longitude = Math.round( location.getLon() + 0.5 );
		else
			longitude = Math.round( location.getLon() );
		int mapLon = new Double( Math.floor( Math.floor( longitude ) / 10.0 ) * 10.0 ).intValue();
		if (mapLon == 180) mapLon = -180;
		final String suffix = mapLon + "_" + mapLat + ".xml?rnd=9165";
		return suffix;
	}

	protected void readGribData( final String fileName ) throws WindResourceFileNotFound {
		try {
			final BufferedReader reader = Files.newBufferedReader( Paths.get( fileName ) );
			final List<WindData> windCellDataList = new Gson().fromJson( reader, new TypeToken<List<WindData>>() {
			}.getType() );
			reader.close();
			this.timeReference = LocalDateTime.now().format( DateTimeFormatter.ofPattern( WIND_DATA_DATE_FORMAT ) );
			this.windMaps.put( timeReference, windCellDataList );
		} catch (final Exception ex) {
			new WindResourceFileNotFound( ex.getMessage(), fileName );
		}
	}

	// - B U I L D E R
	public static class Builder {
		private WindMapHandler onConstruction;

		public Builder() {
			this.onConstruction = new WindMapHandler();
		}

		public WindMapHandler build() {
//			Objects.requireNonNull( this.onConstruction.gribFileName );
			// Try to open the grib file before confirming the creation of the Manager.
//			this.onConstruction.readGribData( this.onConstruction.gribFileName );
			return this.onConstruction;
		}

//		public WindMapHandler.Builder withGRIB2DataFile( final String gribFileName ) {
//			this.onConstruction.gribFileName = Objects.requireNonNull( gribFileName );
//			return this;
//		}
	}
}

//	public void loadWinds( final GeoLocation location ) {
//		final String mapName = this.generateMapName( location );
//
//		try {
//			final URL mapReference = new URL( protocol, host, prefix + mapName );
//			System.out.println( "Loading map data for box " + mapName );
//			final InputStream stream = new BufferedInputStream( mapReference.openStream() );
//			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
//			// - Check for an empty wind map table.
//			if (null == WindMapHandler.windMaps) WindMapHandler.windMaps = new Hashtable<Date, WindMap>();
//			final MapParserHandler handler = new MapParserHandler( WindMapHandler.windMaps );
//			parser.parse( stream, handler );
//		} catch (final MalformedURLException mue) {
//			mue.printStackTrace();
//		} catch (final IOException ioe) {
//			// - Code to create a fake wind map to be used while debugging without line.
//			Date mapDate = Calendar.getInstance().getTime();
//
//			// - Check if we have registered a map with that reference.
//			WindMap currentMap = new WindMap();
//			currentMap.setRef( mapDate.toString() );
//			WindMapHandler.windMaps.put( mapDate, currentMap );
//
//			// - Add the debug data needed.
//			currentMap.addCell( new WindCell( -23, -43, 218, 3.8 ) );
//			currentMap.addCell( new WindCell( -24, -43, 211, 10.3 ) );
//			currentMap.addCell( new WindCell( -24, -42, 203, 11.3 ) );
//			currentMap.addCell( new WindCell( -23, -42, 204, 7.6 ) );
//
//			// - Add the debug data needed.
//			currentMap.addCell( new WindCell( 32, 123, 40, 12.5 ) );
//			currentMap.addCell( new WindCell( 32, 124, 330, 9.8 ) );
//			currentMap.addCell( new WindCell( 32, 125, 70, 11.3 ) );
//			currentMap.addCell( new WindCell( 33, 125, 32, 11.3 ) );
//			currentMap.addCell( new WindCell( 33, 126, 32, 11.3 ) );
//			currentMap.addCell( new WindCell( 34, 126, 349, 8.6 ) );
//
//			// - Add the debug data needed.
//			currentMap.addCell( new WindCell( 1, 104, 16, 9.2 ) );
//			currentMap.addCell( new WindCell( 1, 105, 3, 23.6 ) );
//			currentMap.addCell( new WindCell( 2, 105, 10, 21.0 ) );
//			currentMap.addCell( new WindCell( 2, 106, 9, 22.0 ) );
//			currentMap.addCell( new WindCell( 3, 106, 13, 19.2 ) );
//
//			final Calendar nextMap = Calendar.getInstance();
//			nextMap.add( Calendar.HOUR, 12 );
//			mapDate = nextMap.getTime();
//
//			// - Check if we have registered a map with that reference.
//			currentMap = new WindMap();
//			currentMap.setRef( mapDate.toString() );
//			WindMapHandler.windMaps.put( mapDate, currentMap );
//
//			// - Add the debug data needed.
//			currentMap.addCell( new WindCell( 32, 123, 40, 12.5 ) );
//			currentMap.addCell( new WindCell( 32, 124, 330, 9.8 ) );
//			currentMap.addCell( new WindCell( 32, 125, 120, 11.3 ) );
//			currentMap.addCell( new WindCell( 33, 125, 32, 11.3 ) );
//			currentMap.addCell( new WindCell( 33, 126, 32, 11.3 ) );
//			currentMap.addCell( new WindCell( 34, 126, 349, 8.6 ) );
//
//			// - Add the debug data needed.
//			currentMap.addCell( new WindCell( 1, 104, 16, 9.2 ) );
//			currentMap.addCell( new WindCell( 1, 105, 3, 23.6 ) );
//			currentMap.addCell( new WindCell( 2, 105, 10, 21.0 ) );
//			currentMap.addCell( new WindCell( 2, 106, 9, 22.0 ) );
//			currentMap.addCell( new WindCell( 3, 106, 13, 19.2 ) );
//		} catch (final ParserConfigurationException pce) {
//			pce.printStackTrace();
//		} catch (final SAXException se) {
//			se.printStackTrace();
//		}
//	}


package org.dimensinfin.virtualregatta.routecalculator.windmap.domain;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"firstPoint",
		"secondPoint",
		"windSpeed",
		"windDirection"
})
public class WindData {
	@JsonProperty("firstPoint")
	private WindLocationPoint firstPoint;
	@JsonProperty("secondPoint")
	private WindLocationPoint secondPoint;
	@JsonProperty("longitude")
	private Double longitude;
	@JsonProperty("latitude")
	private Double latitude;
	@JsonProperty("size")
	private Double size;
	@JsonProperty("windSpeed")
	private Double windSpeed;
	@JsonProperty("windDirection")
	private Integer windDirection;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("firstPoint")
	public WindLocationPoint getFirstPoint() {
		return firstPoint;
	}

	@JsonProperty("firstPoint")
	public void setFirstPoint( WindLocationPoint firstPoint ) {
		this.firstPoint = firstPoint;
	}

	@JsonProperty("secondPoint")
	public WindLocationPoint getSecondPoint() {
		return secondPoint;
	}

	@JsonProperty("secondPoint")
	public void setSecondPoint( WindLocationPoint secondPoint ) {
		this.secondPoint = secondPoint;
	}

	public Double getLongitude() {
		return longitude;
	}

	public WindData setLongitude( final Double longitude ) {
		this.longitude = longitude;
		return this;
	}

	public Double getLatitude() {
		return latitude;
	}

	public WindData setLatitude( final Double latitude ) {
		this.latitude = latitude;
		return this;
	}

	public Double getSize() {
		return size;
	}

	public WindData setSize( final Double size ) {
		this.size = size;
		return this;
	}

	@JsonProperty("windSpeed")
	public Double getWindSpeed() {
		return windSpeed;
	}

	@JsonProperty("windSpeed")
	public WindData setWindSpeed( Double windSpeed ) {
		this.windSpeed = windSpeed;
		return this;
	}

	@JsonProperty("windDirection")
	public Integer getWindDirection() {
		return windDirection;
	}

	@JsonProperty("windDirection")
	public WindData setWindDirection( Integer windDirection ) {
		this.windDirection = windDirection;
		return this;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Checks is the setup location fall within the wind cell coverage area.
	 * @param targetLocation the location to test
	 * @return true is the location falls inside the effect area for this cell
	 */
	public boolean contains( final GeoLocation targetLocation ) {
		if ( targetLocation.getLat()>this.latitude+this.size)return false;
		if ( targetLocation.getLat()<this.latitude-this.size)return false;
		if ( targetLocation.getLon()>this.longitude+this.size)return false;
		if ( targetLocation.getLon()<this.longitude-this.size)return false;
		return true;
	}

	@JsonAnySetter
	public void setAdditionalProperty( String name, Object value ) {
		this.additionalProperties.put( name, value );
	}
}

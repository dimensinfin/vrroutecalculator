package org.dimensinfin.virtualregatta.routecalculator.router;

import java.util.List;
import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;
import org.dimensinfin.virtualregatta.routecalculator.io.parser.output.OutputManager;
import org.dimensinfin.virtualregatta.routecalculator.statistic.ProgressReport;

public abstract class Router {
	protected GeoLocation startLocation;
	protected GeoLocation waypoint;
	protected OutputManager outputManager;
	protected RouterConfiguration configuration;
	protected ProgressReport progressReport = new ProgressReport.Builder().build();
	protected RouterType routerType;

	protected Router() {}

	public abstract List<Route> generateRouteSet() throws WindResourceNotFound;

	// - B U I L D E R
	public abstract static class Builder<T extends Router, B> {
		private B actualClassBuilder;

		public Builder() {
			this.actualClassBuilder = getActualBuilder();
		}

		protected abstract T getActual();

		protected abstract B getActualBuilder();

		public T build() {
			Objects.requireNonNull( this.getActual().outputManager );
			Objects.requireNonNull( this.getActual().configuration );
			return this.getActual();
		}

		public B withOutputManager( final OutputManager outputManager ) {
			this.getActual().outputManager = Objects.requireNonNull( outputManager );
			return this.getActualBuilder();
		}

		public B withRouterConfiguration( final RouterConfiguration configuration ) {
			this.getActual().configuration = Objects.requireNonNull( configuration );
			return this.getActualBuilder();
		}

		public B withStartLocation( final GeoLocation startLocation ) {
			this.getActual().startLocation = Objects.requireNonNull( startLocation );
			return this.getActualBuilder();
		}

		public B withWaypoint( final GeoLocation waypoint ) {
			this.getActual().waypoint = Objects.requireNonNull( waypoint );
			return this.getActualBuilder();
		}
	}
}

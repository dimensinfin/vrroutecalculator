package org.dimensinfin.virtualregatta.routecalculator.router;

import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.core.exception.RouteCalculatorException;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;

public abstract class RouteGenerator {
	protected RouterType type;

	public RouterType getType() {
		return this.type;
	}

	public abstract Route newRoute( final GeoLocation start, final Double direction ) throws RouteCalculatorException;

	// - B U I L D E R
	public abstract static class Builder<T extends RouteGenerator, B> {
		private B actualClassBuilder;

		public Builder() {
			this.actualClassBuilder = getActualBuilder();
		}

		protected abstract T getActual();

		protected abstract B getActualBuilder();

		public T build() {
			Objects.requireNonNull( this.getActual().type );
			return this.getActual();
		}
	}
}

package org.dimensinfin.virtualregatta.routecalculator.router;

public class RouterConfiguration {
	private RouterConfiguration() {}

	public Integer getLeftDeviationAngle() {
		return null;
	}

	public Integer getRightDeviationAngle() {
		return null;
	}

	// - B U I L D E R
	public static class Builder {
		private RouterConfiguration onConstruction;

		public Builder() {
			this.onConstruction = new RouterConfiguration();
		}

		public RouterConfiguration build() {
			return this.onConstruction;
		}
	}
}

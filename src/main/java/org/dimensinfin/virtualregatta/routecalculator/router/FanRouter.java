package org.dimensinfin.virtualregatta.routecalculator.router;

import java.util.List;

import org.dimensinfin.virtualregatta.routecalculator.domain.Route;

public class FanRouter extends Router {
	private FanRouter() {super();}

	/**
	 * Runs the route generation process for all the start angles that are configured.
	 * After calculating the loxodromic direction angle then we generate routes to port and to starboard of this angle to start the route fan set.
	 *
	 * @return the list of routes generated.
	 */
	@Override
	public List<Route> generateRouteSet() {
		return null;
	}

	// - B U I L D E R
	public static class Builder extends Router.Builder<FanRouter, FanRouter.Builder> {
		private FanRouter onConstruction;

		public Builder() {
			this.onConstruction = new FanRouter();
		}

		@Override
		protected FanRouter getActual() {
			if (null == this.onConstruction) this.onConstruction = new FanRouter();
			return this.onConstruction;
		}

		@Override
		protected Builder getActualBuilder() {
			return this;
		}

		public FanRouter build() {
			super.build();
			return this.onConstruction;
		}
	}
}

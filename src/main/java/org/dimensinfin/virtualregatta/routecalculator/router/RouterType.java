//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package org.dimensinfin.virtualregatta.routecalculator.router;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum RouterType {
	NONE, DIRECT, FILE, OPTIMDIRECT, OPTIMFILE, ISO, ISOSTEP;

	public static RouterType encode(String argument) {
		if (argument.toUpperCase().equals("NONE")) return NONE;
		if (argument.toUpperCase().equals("DIRECT")) return DIRECT;
		if (argument.toUpperCase().equals("FILE")) return FILE;
		if (argument.toUpperCase().equals("OPTIMDIRECT")) return OPTIMDIRECT;
		if (argument.toUpperCase().equals("OPTIMFILE")) return OPTIMFILE;
		if (argument.toUpperCase().startsWith("ISOSTEP")) return ISOSTEP;
		if (argument.toUpperCase().startsWith("ISO")) return ISO;
		return NONE;
	}
}
// - UNUSED CODE ............................................................................................

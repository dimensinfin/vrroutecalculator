package org.dimensinfin.virtualregatta.routecalculator.router;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.core.config.VMCData;
import org.dimensinfin.virtualregatta.routecalculator.core.config.VORGConstants;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;
import org.dimensinfin.virtualregatta.routecalculator.math.AngleMaths;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;

/**
 * This router will calculate a route between two points using isochrone curves. An isochrone is a curve that joins points at the same time
 * distance from a given point. While a circle joins all the points at the same space distance from a central point, an isochrone joins the points
 * at a same time distance from a start point.
 *
 * The process starts with the start point and a waypoint or set of waypoints. If the number of waypoints is more than 1 then the search for a
 * second and next isochrone curves uses the endpoint of the current route as the start point for the next leg isochrone calculation. This should
 * generate something like a fractal hierarchy explosion. Being each of the suitable step routes as a seed for a new set of isochrone curves.
 *
 * Considering the case for a single waypoint, the process will calculate each of the isochrone curves with the time separation parametrized on the
 * router configuration. After the isochrone calculation reaches the target (or any other end condition configured on the <code>Router</code>) the
 * best route is then searched and sent to the output.
 *
 * The best route is calculated as the route that takes less time to reach the end condition.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.5.0
 */
public class IsochroneRouter extends Router {
	public static final Integer DEFAULT_ISOCRONE_SEPARATION_MINUTES = 15;
	protected Integer isochronePeriod = DEFAULT_ISOCRONE_SEPARATION_MINUTES;
	private Polars polars=new Polars();
	private WindMapHandler windMapHandler;
	private TimedRouteGenerator routeGenerator;
	private List<Route> stepFamilyRoutes = new ArrayList<>();

	private IsochroneRouter() {super(); }

	/**
	 * With the start point and the final waypoint, this method calculates the routes for the different VMGs to
	 * both sides of the resulting angle. Those routes are divided on the wind change points and those points
	 * are recorded to remember the route that has advanced more in the general direction of the start-waypoint
	 * angle.
	 *
	 * The projection line to be used for angle deviations will the straight line that connect the start point and the waypoint. Using that line the
	 * routing process will move to right and left of that angle degree by degree to calculate the list of routes to be generated for evaluation. The
	 * number of grads to deviate is also defined on the Router configuration.
	 *
	 * The process will generate routes using the selected optimization projection algorithm (for example VMG) and those routes will tab internal
	 * points with the time tags at the spaces defined by the configuration. Those tags will be used to later collect the tagged points and to
	 * group them by time so the lines to be generated are not the routes but the connected isochrone points calculated during the route generation.
	 */
	@Override
	public List<Route> generateRouteSet( /*final GeoLocation start, final GeoLocation waypoint */) throws WindResourceNotFound {
		double baseCourse = this.startLocation.angleTo( waypoint ); // Calculate the base angle for the waypoint direction.
		// - Generate the routes using the selected Router
		this.progressReport.startTimer();
		this.progressReport.setMainStepCount(
				Math.abs( this.configuration.getLeftDeviationAngle() ) +
						Math.abs( this.configuration.getRightDeviationAngle() )
		);
		for (int deviation = this.configuration.getLeftDeviationAngle(); deviation <= this.configuration.getRightDeviationAngle(); deviation++) {
			this.progressReport.startStepTimer();
			final int course = (int) AngleMaths.singleton().normalizeAngle( baseCourse + deviation ); // This is the start course for the route
			// Generate a new timed route where the route points reached at specified times are tagged for later use.
			final Route route = this.routeGenerator.newRoute( this.startLocation, (double) course );
			this.stepFamilyRoutes.add( route );
		}
		return this.stepFamilyRoutes;
	}
	public void reportRoute( final String tag, final Route route,final GeoLocation start, final GeoLocation waypoint){
		final StringBuffer header = new StringBuffer();
		header.append( route.getName() ).append( VORGConstants.NEWLINE );
		header.append( "start: " ).append(start.printFormatFull());
		this.outputManager.printHeader(header.toString());
		this.outputManager.printRoute(tag,route);
	}
	// - B U I L D E R
	public static class Builder extends Router.Builder<IsochroneRouter, IsochroneRouter.Builder> {
		private IsochroneRouter onConstruction;

		public Builder() {
			super();
			this.onConstruction = new IsochroneRouter();
		}

		public IsochroneRouter getActual() {
			if (null == this.onConstruction) this.onConstruction = new IsochroneRouter();
			return this.onConstruction;
		}

		public IsochroneRouter.Builder getActualBuilder() {
			return this;
		}

		public IsochroneRouter build() {
			super.build();
			Objects.requireNonNull( this.onConstruction.isochronePeriod );
			Objects.requireNonNull( this.onConstruction.windMapHandler );
			Objects.requireNonNull( this.onConstruction.polars );
			this.onConstruction.routeGenerator = new TimedRouteGenerator.Builder()
					.withIsochroneSeparation( this.onConstruction.isochronePeriod )
					.withWindMapHandler( this.onConstruction.windMapHandler )
					.withBoatPolars( this.onConstruction.polars )
					.build();
			return this.onConstruction;
		}
		public IsochroneRouter.Builder withPolars( final Polars polars ) {
			if (null != polars)	this.onConstruction.polars = Objects.requireNonNull( polars );
			return this;
		}

		public IsochroneRouter.Builder withIsochronePeriod( final Integer isochronePeriod ) {
			if (null != isochronePeriod) this.onConstruction.isochronePeriod = Objects.requireNonNull( isochronePeriod );
			return this;
		}

		public IsochroneRouter.Builder withWindMapHandler( final WindMapHandler windMapHandler ) {
			this.onConstruction.windMapHandler = Objects.requireNonNull( windMapHandler );
			return this.getActualBuilder();
		}
	}
}

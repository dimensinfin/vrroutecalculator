package org.dimensinfin.virtualregatta.routecalculator.router;

import java.text.MessageFormat;
import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteAlgorithmType;
import org.dimensinfin.virtualregatta.routecalculator.domain.TimedRoute;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;

public class TimedRouteGenerator extends RouteGenerator {
	private int isochroneSeparation = 15; // Define how many minutes after tagging a route point with the elapsed route time.
	private Polars boatPolars;
	private WindMapHandler windMapHandler;

	private TimedRouteGenerator() {}

	/**
	 * Generates a new route but with the special characteristic that some special points have to be calculated depending on the route traversing
	 * elapsed time. So instead running until a wall is found or other event, the route is calculated until a specified number of mimutes has
	 * elapsed and that point is recorded on the route.
	 */
	@Override
	public Route newRoute( final GeoLocation start, final Double direction ) throws WindResourceNotFound {
		return new TimedRoute.Builder()
				.withName( MessageFormat.format( "{0} Route - {1}", RouteAlgorithmType.TIMED.name(), direction ) )
				.withPolars( this.boatPolars )
				.withStart( start )
				.withDirection( direction.intValue() )
				.withIsochronePeriod( this.isochroneSeparation ) // This parameter is specific for this type of routes
				.withWindMapHandler( this.windMapHandler )
				.build()
				.generateRoute();
	}

	// - B U I L D E R
	public static class Builder extends RouteGenerator.Builder<TimedRouteGenerator, TimedRouteGenerator.Builder> {
		private TimedRouteGenerator onConstruction;

		public Builder() {
			this.onConstruction = new TimedRouteGenerator();
		}

		public TimedRouteGenerator getActual() {
			if (null == this.onConstruction) this.onConstruction = new TimedRouteGenerator();
			return this.onConstruction;
		}

		public TimedRouteGenerator.Builder getActualBuilder() {
			return this;
		}

		@Override
		public TimedRouteGenerator build() {
			Objects.requireNonNull( this.onConstruction.boatPolars );
			Objects.requireNonNull( this.onConstruction.windMapHandler );
			return this.onConstruction;
		}

		public TimedRouteGenerator.Builder withBoatPolars( final Polars boatPolars ) {
			this.onConstruction.boatPolars = Objects.requireNonNull( boatPolars );
			return this;
		}

		public TimedRouteGenerator.Builder withIsochroneSeparation( final Integer isochroneSeparation ) {
			if (null != isochroneSeparation) this.onConstruction.isochroneSeparation = isochroneSeparation;
			return this;
		}

		public TimedRouteGenerator.Builder withWindMapHandler( final WindMapHandler windMapHandler ) {
			this.getActual().windMapHandler = Objects.requireNonNull( windMapHandler );
			return this.getActualBuilder();
		}
	}
}

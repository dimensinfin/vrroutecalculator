package org.dimensinfin.virtualregatta.routecalculator.core.config;

import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.boat.domain.SailConfiguration;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.math.AngleMaths;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

/**
 * The VMC calculations have changed on version 0.5.0. From a hardcoded VMG calculator now it can be parametrized with the polar boat information
 * so it can generate the correct angles and sail configurations for any system boat.
 *
 * Also instead using a VMC calculator instance for each calculation now the calculator is a single instance configured at creation but that can
 * generate the angle and sail configuration for any new input parameters.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class VMCData {
	protected int targetDirection;
	//	protected final int windDirection;
//	protected final double windSpeed;
	private Polars polars;
	private WindData windData;
	private double leftVMC = 0.0;
	private int leftAngle;
	private SailConfiguration leftConfiguration = new SailConfiguration();
	private double rightVMC = 0.0;
	private int rightAngle;
	private SailConfiguration rightConfiguration = new SailConfiguration();
	private double maxSpeed = 0.0;
	private int maxAngle;
	private SailConfiguration maxConfiguration = new SailConfiguration();
	private int maxAWD;

	private VMCData() {
	}

//	@Deprecated
//	public VMCData( double heading, WindCell startCell ) {
//		this( new Double( heading ).intValue(), startCell );
//	}
//
//	@Deprecated
//	public VMCData( int heading, WindCell startCell ) {
//		this( heading, startCell.getWindDir(), startCell.getWindSpeed() );
//	}
//
//	@Deprecated
//	public VMCData( int heading, int windDirection, double windSpeed ) {
////		targetDirection = heading;
////		this.windDirection = windDirection;
////		this.windSpeed = windSpeed;
////		calculateVMC();
//	}

	public int getWorstAngle() {
		if (leftConfiguration.getSpeed() < rightConfiguration.getSpeed()) return leftAngle;
		else return rightAngle;
	}

	public SailConfiguration getBestSailConfiguration() {
		if (leftConfiguration.getSpeed() >= rightConfiguration.getSpeed())
			return leftConfiguration;
		else
			return rightConfiguration;
	}

	public int getMaxAWD() {
		return Math.abs( maxAWD );
	}

	public Object getMaxSpeed() {
		return maxConfiguration.getSpeed();
	}

	public void addLeftData( double leftVMC, int leftAngle, SailConfiguration leftConfiguration ) {
		this.leftVMC = leftVMC;
		this.leftAngle = leftAngle;
		this.leftConfiguration = leftConfiguration;
	}

	public void addRightData( double rightVMC, int rightAngle, SailConfiguration rightConfiguration ) {
		this.rightVMC = rightVMC;
		this.rightAngle = rightAngle;
		this.rightConfiguration = rightConfiguration;
	}

	public int getBestAngle( final Integer direction ) {
		this.calculateVMC( direction );
		if (this.leftVMC >= this.rightVMC) return this.leftAngle;
		else return this.rightAngle;
	}

	/**
	 * Calculates the boat speed on the setup direction and using the boat polars and wind cell conditions to obtain the real speed.
	 *
	 * @param direction the boat movement direction
	 * @return the boat speed when moving on that direction.
	 */
	public double getSpeed( final int direction ) {
		int angle = AngleMaths.singleton().adjustAngleTo360( this.windData.getWindDirection() + direction );
		final SailConfiguration configuration = this.polars.lookup( direction, this.windData.getWindSpeed() );
		final double speed = configuration.getSpeed();
//		final double vmcSpeed = speed * Math.cos( Math.toRadians( direction - angle ) );
//		if (vmcSpeed > this.rightVMC) {
//			this.rightVMC = vmcSpeed;
//			this.rightAngle = angle;
//			this.rightConfiguration = configuration;
//		}
//		if (speed > this.maxSpeed) {
//			this.maxSpeed = speed;
//			this.maxAngle = angle;
//			this.maxAWD = GeoLocation.calculateAWD( this.windCell.getWindDir(), angle );
//			this.maxConfiguration = configuration;
//		}
		return speed;
	}

	public String printRecord() {
		StringBuffer buffer = new StringBuffer();
//		buffer.append( "VM PORT" ).append( "\t\t\t\t\t" );
//		buffer.append( leftAngle ).append( "\t" );
//		buffer.append( leftConfiguration.getSpeed() ).append( "\t" ).append( leftConfiguration.getSail() ).append( "\t" );
//		buffer.append( windSpeed ).append( "\t" ).append( windDirection ).append( "\t" );
//		buffer.append( leftVMC ).append( "\t" );
//		buffer.append( "\t" ).append( targetDirection ).append( "\n" );/* .append("GMP+1-0H").append("\n"); */
//
//		buffer.append( "VM STAR" ).append( "\t\t\t\t\t" );
//		buffer.append( rightAngle ).append( "\t" );
//		buffer.append( rightConfiguration.getSpeed() ).append( "\t" ).append( rightConfiguration.getSail() ).append( "\t" );
//		buffer.append( windSpeed ).append( "\t" ).append( windDirection ).append( "\t" );
//		buffer.append( rightVMC ).append( "\t" );
//		buffer.append( "\t" ).append( targetDirection ).append( "\n" );/* .append("GMP+1-0H").append("\n"); */
		return buffer.toString();
	}

	public String printReport() {
		StringBuffer buffer = new StringBuffer();
//		int awd = GeoLocation.calculateAWD( windDirection, maxAngle );
//		int portAngle = GeoLocation.adjustAngleTo360( windDirection + getMaxAWD() );
//		int starboardAngle = GeoLocation.adjustAngleTo360( windDirection - getMaxAWD() );
//		buffer.append( "Max Speed=" ).append( maxConfiguration );
//		buffer.append( " course Port=" ).append( portAngle );
//		buffer.append( " course Starboard=" ).append( starboardAngle );
//		buffer.append( " - AWD=+-" ).append( Math.abs( awd ) ).append( VORGConstants.NEWLINE );
//		buffer.append( "[VMC results" ).append( VORGConstants.NEWLINE );
//		buffer.append( "   Projection heading=" ).append( targetDirection ).append( VORGConstants.NEWLINE );
//		buffer.append( "   Wind direction=" ).append( windDirection ).append( VORGConstants.NEWLINE );
//		buffer.append( "   Wind speed=" ).append( FormatSingletons.nf3.format( windSpeed ) ).append( " knots" ).append(
//				VORGConstants.NEWLINE );
//		buffer.append( "   Starboard VMC [boat speed=" ).append( FormatSingletons.nf3.format( leftConfiguration.getSpeed() ) );
//		buffer.append( "-" ).append( leftConfiguration.getSail() ).append( "] [VMC=" );
//		buffer.append( FormatSingletons.nf3.format( leftVMC ) ).append( " knots - heading=" );
//		buffer.append( leftAngle );
//		awd = GeoLocation.calculateAWD( windDirection, leftAngle );
//		buffer.append( " AWD=" ).append( awd * Integer.signum( awd ) );
//		buffer.append( "]" ).append( VORGConstants.NEWLINE );
//		buffer.append( "   Port VMC [boat speed=" ).append( FormatSingletons.nf3.format( rightConfiguration.getSpeed() ) );
//		buffer.append( "-" ).append( rightConfiguration.getSail() );
//		buffer.append( "] [VMC=" ).append( FormatSingletons.nf3.format( rightVMC ) ).append( " knots - heading=" );
//		buffer.append( rightAngle );
//		awd = GeoLocation.calculateAWD( windDirection, rightAngle );
//		buffer.append( " AWD=" ).append( awd * Integer.signum( awd ) );
//		buffer.append( "]" ).append( VORGConstants.NEWLINE ).append( "]" );
		return buffer.toString();
	}

	@Override
	public String toString() {
		return printReport();
	}

	/**
	 * Scans all angles to store in the internal fields the values for the port, starboard and max speed VMC. Uses the current wind conditions and
	 * also the boat polars to get the speed and sail configurations for any requested angle.
	 */
	private void calculateVMC( final int direction ) {
		for (int rotation = 1; rotation < 180; rotation++) { // Starboard rotation
			int angle = AngleMaths.singleton().adjustAngleTo360( this.windData.getWindDirection() + rotation );
			final SailConfiguration configuration = this.polars.lookup( rotation, this.windData.getWindSpeed() );
			final double speed = configuration.getSpeed();
			final double vmcSpeed = speed * Math.cos( Math.toRadians( direction - angle ) );
			if (vmcSpeed > this.rightVMC) {
				this.rightVMC = vmcSpeed;
				this.rightAngle = angle;
				this.rightConfiguration = configuration;
			}
			if (speed > this.maxSpeed) {
				this.maxSpeed = speed;
				this.maxAngle = angle;
				this.maxAWD = GeoLocation.calculateAWD( this.windData.getWindDirection(), angle );
				this.maxConfiguration = configuration;
			}
		}
		for (int rotation = 1; rotation < 180; rotation++) { // Port rotation
			int angle = GeoLocation.adjustAngleTo360( this.windData.getWindDirection() - rotation );
			SailConfiguration configuration = new Polars().lookup( rotation, this.windData.getWindSpeed() );
			double speed = configuration.getSpeed();
			double vmcSpeed = speed * Math.cos( Math.toRadians( direction - angle ) );
			if (vmcSpeed > this.leftVMC) {
				this.leftVMC = vmcSpeed;
				this.leftAngle = angle;
				this.leftConfiguration = configuration;
			}
			if (speed > this.maxSpeed) {
				this.maxSpeed = speed;
				this.maxAngle = angle;
				this.maxAWD = GeoLocation.calculateAWD( this.windData.getWindDirection(), angle );
				this.maxConfiguration = configuration;
			}
		}
	}

	// - B U I L D E R
	public static class Builder {
		private VMCData onConstruction;

		public Builder() {
			this.onConstruction = new VMCData();
		}

		public VMCData build() {
			Objects.requireNonNull( this.onConstruction.polars );
			Objects.requireNonNull( this.onConstruction.windData );
			return this.onConstruction;
		}

		public VMCData.Builder withPolars( final Polars polars ) {
			this.onConstruction.polars = Objects.requireNonNull( polars );
			return this;
		}

		public VMCData.Builder withWindData( final WindData windData ) {
			this.onConstruction.windData = Objects.requireNonNull( windData );
			return this;
		}
	}

}

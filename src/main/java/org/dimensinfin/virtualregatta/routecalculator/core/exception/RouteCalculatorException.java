package org.dimensinfin.virtualregatta.routecalculator.core.exception;

public class RouteCalculatorException extends Exception{
	public RouteCalculatorException( final String message ) {
		super(message);
	}
}

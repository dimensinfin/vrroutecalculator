package org.dimensinfin.virtualregatta.routecalculator.core.exception;

public class WindResourceFileNotFound extends RouteCalculatorException {

	public WindResourceFileNotFound( final String message, final String fileName ) {
		super( ErrorInfo.WIND_GRIB_FILE_ACCESS_ERROR.getErrorMessage(
				message, fileName )
		);
	}
}

package org.dimensinfin.virtualregatta.routecalculator.core.config;

public class VORGConstants {
	public static final double TOMINUTES = 60.0;
	public static final double EARTHRADIUS = 3437.74;
	public static final String VERSION = "Version Beta 0.4.4b 10/04";
	public static final int NOCONFIG = 10;
	public static final int INVALIDCONFIGURATION = 11;
	public static final int GENERICERROR = 12;
	public static final double MIN_CELL_DISTANCE = 0.0001;
	public static final String NEWLINE = System.getProperty( "line.separator" );
}

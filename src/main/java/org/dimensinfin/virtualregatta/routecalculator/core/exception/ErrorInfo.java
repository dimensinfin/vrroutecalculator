package org.dimensinfin.virtualregatta.routecalculator.core.exception;

import java.text.MessageFormat;

public enum ErrorInfo {
	INVALID_CELL_INTERSECTION_CALCULATION( "Expected intersections on wind cell boundaries [{0}, {1}, {2}, {3}] from position [{4}, " +
			"{5}] moving direction {6}." ),
	WIND_CELL_NOT_FOUND("The wind cell containing location [{0}, {1}] not found. {4}"),
	WIND_GRIB_FILE_ACCESS_ERROR("Exception {0} while openning GRIB2 wind data file {1}");
	public final String errorMessage;

	ErrorInfo( final String errorMessage ) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage( final Object... arguments ) {
		return MessageFormat.format( this.errorMessage, arguments );
	}
}

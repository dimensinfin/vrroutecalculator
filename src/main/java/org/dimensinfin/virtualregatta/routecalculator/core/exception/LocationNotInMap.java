package org.dimensinfin.virtualregatta.routecalculator.core.exception;

public class LocationNotInMap extends RouteCalculatorException {
	public LocationNotInMap( final String message ) {
		super(message);
	}
}

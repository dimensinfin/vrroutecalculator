package org.dimensinfin.virtualregatta.routecalculator.core.exception;

import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;

public class WindResourceNotFound extends RouteCalculatorException {

	public WindResourceNotFound( final GeoLocation location, final String cause ) {
		super( ErrorInfo.WIND_CELL_NOT_FOUND.getErrorMessage(
				location.getLat(),
				location.getLon(),
				cause )
		);
	}
}

package org.dimensinfin.virtualregatta.routecalculator.statistic;

import java.time.Instant;

public class ProgressReport {
	private Instant startInstant;
	private Instant startStepInstant;
	private int mainStepCount = 0;

	private ProgressReport() {}

	public void setMainStepCount( final int count ) {
		this.mainStepCount = Math.abs( count );
	}

	public void startTimer() {
		this.startInstant = Instant.now();
	}
	public void startStepTimer() {
		this.startStepInstant = Instant.now();
	}

	// - B U I L D E R
	public static class Builder {
		private ProgressReport onConstruction;

		public Builder() {
			this.onConstruction = new ProgressReport();
		}

		public ProgressReport build() {
			return this.onConstruction;
		}
	}
}

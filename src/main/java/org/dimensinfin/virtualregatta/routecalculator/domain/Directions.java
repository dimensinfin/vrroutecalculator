package org.dimensinfin.virtualregatta.routecalculator.domain;

public enum Directions {
	N, NE, E, SE, S, SW, W, NW, NS, EW;
}

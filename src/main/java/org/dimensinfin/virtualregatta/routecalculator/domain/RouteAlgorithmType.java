package org.dimensinfin.virtualregatta.routecalculator.domain;

public enum RouteAlgorithmType {
	VMG, TIMED
}

package org.dimensinfin.virtualregatta.routecalculator.domain;

/**
 * Boundary defines the space values for the lines that define the four sides (north, east, south, west) of a box. The values are 4 coordinates, 2
 * longitude (east, west) and two latitude (north, south) expressed in degrees real notation.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class Boundaries {
	private double north = 0.0;
	private double south = 0.0;
	private double west = 0.0;
	private double east = 0.0;

	public Boundaries() {
	}

	public double getEast() {
		return this.east;
	}

	@Deprecated
	public void setEast( final double location ) {
		east = location;
	}

	public double getNorth() {
		return this.north;
	}

	@Deprecated
	public void setNorth( final double location ) {
		north = location;
	}

	public double getSouth() {
		return this.south;
	}

	@Deprecated
	public void setSouth( final double location ) {
		south = location;
	}

	public double getWest() {
		return this.west;
	}

	@Deprecated
	public void setWest( final double location ) {
		west = location;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "[Boundaries " );
		buffer.append( "north=" ).append( north ).append( "," );
		buffer.append( "east=" ).append( east ).append( "," );
		buffer.append( "south=" ).append( south ).append( "," );
		buffer.append( "west=" ).append( west ).append( "]" );
		return buffer.toString();
	}
}

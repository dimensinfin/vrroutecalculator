package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.Polars;
import org.dimensinfin.virtualregatta.routecalculator.router.RouteGenerator;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;

/**
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public abstract class Route {
	protected RouteAlgorithmType type;
	protected String name;
//	protected RouteGenerator generator;
	protected Polars polars;
	protected WindMapHandler windMapHandler;

	protected Route() {}

	public RouteAlgorithmType getType() {
		return this.type;
	}

	public String getName() {
		return this.name;
	}

	public Route setName( final String name ) {
		this.name = name;
		return this;
	}

	public abstract String toPrinter();

//	public RouteGenerator getGenerator() {
//		return this.generator;
//	}

	// - B U I L D E R
	public abstract static class Builder<T extends Route, B> {
		private B actualClassBuilder;

		public Builder() {
			this.actualClassBuilder = getActualBuilder();
		}

		protected abstract T getActual();

		protected abstract B getActualBuilder();

		public T build() {
			Objects.requireNonNull( this.getActual().type );
			Objects.requireNonNull( this.getActual().name );
			Objects.requireNonNull( this.getActual().polars );
//			Objects.requireNonNull( this.getActual().generator );
			Objects.requireNonNull( this.getActual().windMapHandler );
			return this.getActual();
		}

		@Deprecated
		public B withAlgorithmType( final RouteAlgorithmType type ) {
			this.getActual().type = type;
			return this.getActualBuilder();
		}
		public B withName( final String name ) {
			this.getActual().name = Objects.requireNonNull( name );
			return this.getActualBuilder();
		}

		public B withPolars( final Polars polars ) {
			this.getActual().polars = Objects.requireNonNull( polars );
			return this.getActualBuilder();
		}

		public B withWindMapHandler( final WindMapHandler windMapHandler ) {
			this.getActual().windMapHandler = Objects.requireNonNull( windMapHandler );
			return this.getActualBuilder();
		}
	}
}

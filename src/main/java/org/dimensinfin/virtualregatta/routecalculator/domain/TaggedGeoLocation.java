package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.util.Objects;

public class TaggedGeoLocation {
	private GeoLocation location;
	private String tag;

	private TaggedGeoLocation() {}

	public GeoLocation getLocation() {
		return location;
	}

	public String getTag() {
		return tag;
	}

	// - B U I L D E R
	public static class Builder {
		private TaggedGeoLocation onConstruction;

		public Builder() {
			this.onConstruction = new TaggedGeoLocation();
		}
		public TaggedGeoLocation.Builder withLocation ( final GeoLocation location){
			this.onConstruction.location= Objects.requireNonNull(location);
			return this;
		}
		public TaggedGeoLocation.Builder withTag ( final String tag){
			this.onConstruction.tag= Objects.requireNonNull(tag);
			return this;
		}
		public TaggedGeoLocation build() {
			Objects.requireNonNull( this.onConstruction.location );
			Objects.requireNonNull( this.onConstruction.tag );
			return this.onConstruction;
		}
	}
}

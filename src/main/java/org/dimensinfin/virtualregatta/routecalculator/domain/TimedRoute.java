package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.dimensinfin.virtualregatta.routecalculator.core.config.VMCData;
import org.dimensinfin.virtualregatta.routecalculator.core.exception.WindResourceNotFound;
import org.dimensinfin.virtualregatta.routecalculator.math.LengthUnit;
import org.dimensinfin.virtualregatta.routecalculator.math.RealTimeUnit;
import org.dimensinfin.virtualregatta.routecalculator.math.SpeedUnit;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindData;

/**
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.5.0
 */
public class TimedRoute extends Route {
//	protected VMCData boatVMC; // Changes when the wind cell changes because it contains the wind conditions.
	/**
	 * This is the instant the route generation starts. It is used as the base point to calculate partial runs and also to obtain the right wind
	 * cell from the Wind Manager.
	 */
	protected Instant startTime;
	protected Duration elapsedRouteTime;
	private final List<TaggedGeoLocation> taggedLocations = new ArrayList<>();
	private Integer isochronePeriod; // The time in minutes between partial runs to generate isochrone points.
	private GeoLocation startLocation;
	private Integer direction;

	private TimedRoute() {
		this.type = RouteAlgorithmType.TIMED;
	}

	public Integer getDirection() {
		return this.direction;
	}

	public Integer getIsochronePeriod() {
		return this.isochronePeriod;
	}

	public List<TaggedGeoLocation> getRouteLocations() {
		return this.taggedLocations;
	}

	/**
	 * Generates a partial route along the setup angle that runs for the selected time. The start point is a map location that is the route base.
	 * The resulting route will traverse one or more cells up to the boat location reached or completed the end route condition.
	 *
	 * Each partial run will have to travel on the setup direction until the next isochrone time point is reached. Because isochrones connect
	 * points at the same time distance the parameter that determines the sub step end is the time.
	 *
	 * Time starts at the now instant and for a partial run spans up to the minutes setup by the Route Generator
	 * parameter <code>isochroneSeparation</code>
	 */
	public Route generateRoute() throws WindResourceNotFound {
		this.startRoute();
		GeoLocation nextLocation = this.startLocation;
		while (!this.evaluateEndCondition()) {
			nextLocation = this.calculateNextIsochronePoint( nextLocation, this.direction, this.getIsochronePeriod() );
		}
		return this;
	}

	@Override
	public String toPrinter() {
		final StringBuffer output = new StringBuffer();
		for (TaggedGeoLocation location : this.taggedLocations) {
			output.append( MessageFormat.format( "{0} - [{1} - {2}]\n", location.getTag(),
					location.getLocation().formatLatitude(),
					location.getLocation().formatLongitude() )
			);
		}
		return output.toString();
	}

	/**
	 * This method will adjust the time record to the time zone understand by the wind cell handler. With this information we can retrieve the
	 * right cell and with the correct timed wind data to perform the boat polar calculations.
	 *
	 * @return the adjusted wind cell time coordinates of the current boat location.
	 */
	protected ZonedDateTime accessWindCellSearchTime() {
		return this.startTime.plus( this.elapsedRouteTime ).atZone( ZoneId.of( "UTC" ) );
	}

	protected GeoLocation calculateNextIsochronePoint( final GeoLocation boatLocation,
	                                                   final int boatDirection,
	                                                   final int nextIsochronePeriod ) throws WindResourceNotFound {
		final WindData windData = this.windMapHandler.windData4Location( boatLocation, Optional.of( this.accessWindCellSearchTime() ) );
		final VMCData boatVMC = new VMCData.Builder()
				.withPolars( this.polars )
				.withWindData( windData )
				.build();
		final GeoLocation newBoatPosition = this.run4Time( boatLocation, boatVMC.getSpeed( boatDirection ),
				boatDirection, nextIsochronePeriod );
		final String tag = this.elapsedRouteTime.toString();
		this.taggedLocations.add( new TaggedGeoLocation.Builder()
				.withLocation( newBoatPosition )
				.withTag( tag )
				.build() );
		return newBoatPosition;
	}

	protected boolean evaluateEndCondition() {
		return 0 < this.elapsedRouteTime.compareTo( Duration.ofMinutes( 90 ) );
	}

	/**
	 * Moved the boat on the setup direction for the specified time at the current boat speed to calculate the new boat location. Does not use
	 * spherical trigonometry since the distance to travel is quite short and there are no differences on the calculation results.
	 *
	 * @param location   the current boat location geo coordinates
	 * @param speed      the angle to move the boat
	 * @param minutesRun the number of minutes of the movement
	 * @return the new boat location
	 */
	protected GeoLocation run4Time( final GeoLocation location, final double speed, final int travelDirection, final int minutesRun ) {
		final double distanceTraveled = SpeedUnit.KNOTS.toKMH( speed ) * RealTimeUnit.MINUTES.toHours( (double) minutesRun ); // Distance in KM
		final Double deltaLatitude = LengthUnit.KM.toLatitude( distanceTraveled * Math.cos( Math.toRadians( travelDirection ) ) );
		final Double deltaLongitude = LengthUnit.KM.toLongitude( distanceTraveled * Math.sin( Math.toRadians( travelDirection ) ),
				location.latitude );
		this.elapsedRouteTime = this.elapsedRouteTime.plus( Duration.ofMinutes( minutesRun ) );
		return new GeoLocation.Builder()
				.withLatitude( location.latitude - deltaLatitude )
				.withLongitude( location.longitude + deltaLongitude )
				.build();
	}

	protected void startRoute() {
		this.startTime = Instant.now();
		this.elapsedRouteTime = Duration.ZERO;
	}

	// - B U I L D E R
	public static class Builder extends Route.Builder<TimedRoute, TimedRoute.Builder> {
		private TimedRoute onConstruction;

		@Override
		protected TimedRoute getActual() {
			if (null == this.onConstruction) this.onConstruction = new TimedRoute();
			return this.onConstruction;
		}

		@Override
		protected TimedRoute.Builder getActualBuilder() {
			return this;
		}

		@Override
		public TimedRoute build() {
			super.build();
			Objects.requireNonNull( this.onConstruction.startLocation );
			Objects.requireNonNull( this.onConstruction.direction );
			Objects.requireNonNull( this.onConstruction.isochronePeriod );
			return this.onConstruction;
		}

		public TimedRoute.Builder withDirection( final Integer direction ) {
			this.onConstruction.direction = Objects.requireNonNull( direction );
			return this;
		}

		public TimedRoute.Builder withIsochronePeriod( final Integer isochronePeriod ) {
			this.onConstruction.isochronePeriod = Objects.requireNonNull( isochronePeriod );
			return this;
		}

		public TimedRoute.Builder withStart( final GeoLocation startLocation ) {
			this.onConstruction.startLocation = Objects.requireNonNull( startLocation );
			return this;
		}
	}
}

package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Formatter;
import java.util.Locale;
import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.core.config.VORGConstants;
import org.dimensinfin.virtualregatta.routecalculator.math.AngleMaths;

/**
 * This class represent a point in the geographical world coordinates and has the mathematical methods to
 * perform some calculations on that space. The mapping is done to an sphere that represents the Earth.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class GeoLocation {
	/**
	 * Reduces the parameter angle to be always between 0 and 360. Negative angles are complemented.
	 * This version accepts angles in double precision.
	 *
	 * @param heading the input heading angle
	 * @return the equivalent angle but between 0 and 360 in the positive values.
	 */
	@Deprecated
	public static double adjustAngleTo360( final double heading ) {
		if (heading == 0.0) return 360.0;
		if (heading > 360.0) return GeoLocation.adjustAngleTo360( heading - 360.0 );
		if (heading < 0.0) return GeoLocation.adjustAngleTo360( heading + 360.0 );
		return heading;
	}

	/**
	 * Reduces the parameter angle to be always between 0 and 360. Negative angles are complemented.
	 * This version accepts angles in integer precision.
	 *
	 * @param heading the input heading angle
	 * @return the equivalent angle but between 0 and 360 in the positive values.
	 */
	@Deprecated
	public static int adjustAngleTo360( final int heading ) {
		if (heading == 0) return 360;
		if (heading > 360) return GeoLocation.adjustAngleTo360( heading - 360 );
		if (heading < 0) return GeoLocation.adjustAngleTo360( heading + 360 );
		return heading;
	}

	/**
	 * Calculate the difference in grades between two angles taking on account the 0-360 rose wind.
	 */
	@Deprecated
	public static double angleDifference( final double alpha1, final double alpha2 ) {
		return new AngleMaths().angleDifference( alpha1, alpha2 );
	}

	@Deprecated
	public static int angleDifference( final int angle1, final int angle2 ) {
		return (int) GeoLocation.angleDifference( (double) angle1, (double) angle2 );
	}

	/**
	 * Calculate the AWd between the course of the boat and the wind direction. We have to take care of the
	 * limit when the boat angle and the wind are at different sides of the 0-360 course.
	 */
	@Deprecated
	public static int calculateAWD( final double windAngle, final double boatAngle ) {
		return new AngleMaths().calculateAWD( windAngle, boatAngle );
	}

	// - F I E L D - S E C T I O N ............................................................................
	protected double latitude = 50.0;
	protected double longitude = 0.0;

	// - C O N S T R U C T O R S
	@Deprecated
	public GeoLocation() {
	}

	@Deprecated
	public GeoLocation( final double lat, final double lon ) {
		this.latitude = lat;
		this.longitude = lon;
		// - BUG Related to the crossing of the date line.
		if (Math.abs( this.longitude ) > 180.0) this.longitude -= 360.0 * Math.signum( this.longitude );
	}

	@Deprecated
	public GeoLocation( final int latGrade, final int latMinute, final int lonGrade, final int lonMinute ) {
		if (latGrade < 0)
			latitude = latGrade * 1.0 - latMinute / 60.0;
		else
			latitude = latGrade * 1.0 + latMinute / 60.0;
		if (lonGrade < 0)
			longitude = lonGrade * 1.0 - lonMinute / 60.0;
		else
			longitude = lonGrade * 1.0 + lonMinute / 60.0;
		// - BUG Related to the crossing of the date line.
		if (Math.abs( longitude ) > 180.0) longitude -= 360.0 * Math.signum( longitude );
	}

	public double getLat() {
		return latitude;
	}

//	@Deprecated
//	public void setLat( final double lat ) {
//		latitude = lat;
//	}

//	/**
//	 * Set the latitude from an string supporting the integer format, the double format of the sexagesimal
//	 * special format for degrees separated from minuted by the character ":".
//	 */
//	@Deprecated
//	public void setLat( final String newLatitude ) {
//		if (null == newLatitude) {
//			this.setLat( 0.0 );
//			return;
//		}
//		try {
//			// - Divide the data into the degree and the minutes.
//			final int pos = newLatitude.indexOf( ":" );
//			// - The longitude comes in double format
//			if (pos < 1)
//				this.setLat( new Double( newLatitude ).doubleValue() );
//			else {
//				final int degree = new Integer( newLatitude.substring( 0, pos ) ).intValue();
//				final int minute = new Integer( newLatitude.substring( pos + 1, newLatitude.length() ) ).intValue();
//				// - Test for negative values.
//				if (degree < 0)
//					this.setLat( degree * 1.0 - minute / 60.0 );
//				else
//					this.setLat( degree * 1.0 + minute / 60.0 );
//			}
//		} catch (final Exception ex) {
//			this.setLat( 0.0 );
//		}
//	}

	public double getLon() {
		return longitude;
	}
//@Deprecated
//	public void setLon( final double lon ) {
//		longitude = lon;
//		// - BUG Related to the crossing of the date line.
//		if (Math.abs( longitude ) > 180.0) longitude -= 360.0 * Math.signum( longitude );
//	}
//
//	/**
//	 * Set the longitude from an string supporting the integer format, the double format of the sezagesimal
//	 * special format for degrees separated from minuted by the character ":".
//	 */
//	@Deprecated
//	public void setLon( final String newLongitude ) {
//		if (null == newLongitude) {
//			this.setLon( 0.0 );
//			return;
//		}
//		try {
//			// - Divide the data into the degree and the minutes.
//			final int pos = newLongitude.indexOf( ":" );
//			// - The longitude comes in double format
//			if (pos < 1)
//				this.setLon( new Double( newLongitude ).doubleValue() );
//			else {
//				final int degree = new Integer( newLongitude.substring( 0, pos ) ).intValue();
//				final int minute = new Integer( newLongitude.substring( pos + 1, newLongitude.length() ) ).intValue();
//				// - Test for negative values.
//				if (degree < 0)
//					this.setLon( degree * 1.0 - minute / 60.0 );
//				else
//					this.setLon( degree * 1.0 + minute / 60.0 );
//			}
//		} catch (final Exception ex) {
//			this.setLon( 0.0 );
//		}
//	}

//	@Deprecated
//	public void setLocation( final GeoLocation newLocation ) {
//		this.setLat( newLocation.getLat() );
//		this.setLon( newLocation.getLon() );
//		//		// - BUG Related to the crossing of the date line.
//		//		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
//	}

	/**
	 * This method calculates the angle that forms a destination point with a start point. The resulting value
	 * is a positive integer between 0-360. Normalization is also performed inside this call.
	 *
	 * @param destination the angle to point to go to the destination. The source point if the current location represented by this instance.
	 * @return the corrected spherical angle to point to reach the destination point. Accounts for spherical trigonometry.
	 */
	@Deprecated
	public double angleTo( final GeoLocation destination ) {
		double alpha = 0.0;
		double deltaLon = destination.getLon() - getLon();
		// - Control the change over the date line.
		if (Math.abs( deltaLon ) > 180.0) deltaLon = 360.0 - (Math.abs( deltaLon ) * Math.signum( deltaLon ));
		deltaLon *= 60.0;

		// - Calculate the exaggerated latitude and then the loxodromic distance and angle.
		final double Bs = 3437.746 * Math.log( Math.tan( Math.toRadians( 45 + getLat() / 2.0 ) ) ) - 23.0
				* Math.sin( Math.toRadians( getLat() ) );
		final double Be = 3437.746 * Math.log( Math.tan( Math.toRadians( 45 + destination.getLat() / 2.0 ) ) ) - 23.0
				* Math.sin( Math.toRadians( destination.getLat() ) );
		final double deltaLat = Be - Bs;

		// - Calculate now the loxodromic angle.
		alpha = Math.toDegrees( Math.atan( deltaLon / deltaLat ) );
		if (deltaLat > 0) alpha = alpha + 360.0;
		if (deltaLat < 0) alpha = alpha + 180.0;

		return GeoLocation.adjustAngleTo360( alpha );
	}

	public double courseAngle( final GeoLocation destination ) {
		var rlat0 = Math.toRadians( this.getLat() );
		var rlat1 = Math.toRadians( destination.getLat() );
		var rlon0 = Math.toRadians( this.getLon() );
		var rlon1 = Math.toRadians( destination.getLon() );
		var xi = this.gcAngle( Math.toRadians( this.getLat() ), rlon0, rlat1, rlon1 );
		var a = Math.acos( (Math.sin( rlat1 ) - Math.sin( rlat0 ) * Math.cos( xi )) / (Math.cos( rlat0 ) * Math.sin( xi )) );
		return (Math.sin( rlon1 - rlon0 ) > 0) ? a : (2 * Math.PI - a);
	}

	/**
	 * This method calculates a new location based on the direct estimation for a loxodromic curve applied to a
	 * starting point. The distance is directly received as a parameter and the same for the moving course.
	 */
	public GeoLocation directEstimation( final double distance, final double course ) {
		final double endLat = getLat() * 60 + distance * Math.cos( Math.toRadians( course ) );

		final double Bs = 3437.746 * Math.log( Math.tan( Math.toRadians( 45 + getLat() / 2.0 ) ) ) - 23.0
				* Math.sin( Math.toRadians( getLat() ) );
		final double Be = 3437.746 * Math.log( Math.tan( Math.toRadians( 45 + (endLat / 60.0) / 2.0 ) ) ) - 23.0
				* Math.sin( Math.toRadians( endLat / 60.0 ) );
		final double deltaLat = Be - Bs;
		double dlo;
		if ((course == 90) || (course == 270)) {
			dlo = distance / Math.cos( Math.abs( Math.toRadians( getLat() ) ) );
			if (course == 270) dlo = -dlo;
		} else
			dlo = deltaLat * Math.tan( Math.toRadians( course ) );

		final double endLon = getLon() * 60 + dlo;
		return new GeoLocation( endLat / 60.0, endLon / 60.0 );
	}

	/**
	 * This method calculates a new location based on the direct estimation for a loxodromic curve applied to a
	 * starting point. The distance to move is obtained from the time, the speed and the course angle.
	 */
	public GeoLocation directEstimation( final double seconds, final double speed, final double course ) {
		return this.directEstimation( speed * seconds / (60.0 * 60.0), course );
	}

	public double distance( final GeoLocation destination ) {
		final double deltaLat = destination.getLat() - getLat();
		final double deltaLon = destination.getLon() - getLon();
		final double hyp = Math.hypot( deltaLat, deltaLon );
		final double Ls = Math.toRadians( getLat() );
		final double Ld = Math.toRadians( destination.getLat() );
		final double ldelta = Math.toRadians( deltaLon );
		final double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos( Math.sin( Ls ) * Math.sin( Ld ) + Math.cos( Ls ) * Math.cos( Ld ) * Math.cos( ldelta ) );
		return loxDistance;
	}

	public String formatLatitude() {
		int latDegree;
		long latMinute;
		if (latitude < 0) {
			latDegree = new Double( Math.floor( latitude * -1.0 ) ).intValue();
			latMinute = Math.round( ((latitude * -1.0) - Math.floor( latitude * -1.0 )) * 60.0 );
		} else {
			latDegree = new Double( Math.floor( latitude ) ).intValue();
			latMinute = Math.round( (latitude - Math.floor( latitude )) * 60.0 );
		}
		// - Generate the output.
		final Formatter formatter = new Formatter( Locale.ENGLISH );
		formatter.format( "%1$02d %2$02d'", latDegree, latMinute );
		if (latitude > 0)
			return formatter + " N";
		else
			return formatter + " S";
	}

	public String formatLongitude() {
		int lonDegree;
		long lonMinute;
		if (longitude < 0) {
			lonDegree = new Double( Math.floor( longitude * -1.0 ) ).intValue();
			lonMinute = Math.round( ((longitude * -1.0) - Math.floor( longitude * -1.0 )) * 60.0 );
		} else {
			lonDegree = new Double( Math.floor( longitude ) ).intValue();
			lonMinute = Math.round( (longitude - Math.floor( longitude )) * 60.0 );
		}
		Formatter formatter = new Formatter( Locale.ENGLISH );
		formatter = new Formatter( Locale.ENGLISH );
		formatter.format( "%1$03d %2$02d'", lonDegree, lonMinute );
		if (longitude > 0)
			return formatter + " E";
		else
			return formatter + " W";
	}

	public String formattedLocation() {
		return formatLatitude() + " - " + formatLongitude();
	}

	public boolean isEquivalent( final GeoLocation targetLocation ) {
		if (Math.abs( targetLocation.getLat() - getLat() ) < VORGConstants.MIN_CELL_DISTANCE)
			if (Math.abs( targetLocation.getLon() - getLon() ) < VORGConstants.MIN_CELL_DISTANCE) return true;
		return false;
	}

	public String printFormatFull() {
		return MessageFormat.format( "{0}º{1}'{2}''", (int) this.getLat() );
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "[GeoLocation " );
		final NumberFormat nf = NumberFormat.getInstance( Locale.ENGLISH );
		nf.setMaximumFractionDigits( 4 );
		nf.setMinimumFractionDigits( 4 );
		buffer.append( "lat=" ).append( nf.format( latitude ) ).append( "," );
		buffer.append( "lon=" ).append( nf.format( longitude ) ).append( "]" );
		buffer.append( " [" ).append( formattedLocation().replace( '\t', ' ' ).replace( '�', ' ' ) ).append( "]" );
		return buffer.toString();
	}

	protected double gcAngle( final double rlat0, final double rlon0, final double rlat1, final double rlon1 ) {
		return Math.acos( Math.sin( rlat0 ) * Math.sin( rlat1 ) + Math.cos( rlat0 ) * Math.cos( rlat1 ) * Math.cos( rlon1 - rlon0 ) );
	}

	/**
	 * This method calculates the angle that forms a destination point with a start point. The resulting value
	 * may be a positive or a negative number. 0-360 normalization should be performed at the call level.
	 */
	@Deprecated
	private double angleToTrig( final GeoLocation destination ) {
		double alpha = 0.0;
		final double deltaLat = destination.getLat() - getLat();
		double deltaLon = destination.getLon() - getLon();
		// - Control the change over the date line.
		if (Math.abs( deltaLon ) > 180.0) deltaLon = 360.0 - Math.abs( deltaLon );
		final double hyp = Math.hypot( deltaLat, deltaLon );

		// - In the case the origin and destination are the same we return by default the angle 0.
		if ((deltaLat == 0.0) && (deltaLon == 0.0)) return 0.0;

		Quadrants quadrant = Quadrants.QUADRANT_I;
		// - Detect the angle quadrant.
		if ((deltaLat >= 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_I;
		if ((deltaLat < 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_II;
		if ((deltaLat < 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_III;
		if ((deltaLat >= 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_IV;

		switch (quadrant) {
			case QUADRANT_I:
				// -Use the bigger value to have a more exact result.
				if (Math.abs( deltaLon ) > Math.abs( deltaLat ))
					alpha = Math.asin( deltaLon / hyp );
				else
					alpha = Math.acos( deltaLat / hyp );
				break;
			case QUADRANT_II:
				// -Use the bigger value to have a more exact result.
				if (Math.abs( deltaLon ) > Math.abs( deltaLat ))
					alpha = Math.PI - Math.asin( deltaLon / hyp );
				else
					alpha = Math.acos( deltaLat / hyp );
				break;
			case QUADRANT_III:
				// -Use the bigger value to have a more exact result.
				if (Math.abs( deltaLon ) > Math.abs( deltaLat ))
					alpha = Math.PI - Math.asin( deltaLon / hyp );
				else
					alpha = 2.0 * Math.PI - Math.acos( deltaLat / hyp );
				break;
			case QUADRANT_IV:
				// -Use the bigger value to have a more exact result.
				if (Math.abs( deltaLon ) > Math.abs( deltaLat ))
					alpha = Math.asin( deltaLon / hyp );
				else
					alpha = Math.acos( deltaLat / hyp ) * -1.0;
				break;

			default:
				break;
		}

		return GeoLocation.adjustAngleTo360( Math.toDegrees( alpha ) );
	}

	// - B U I L D E R
	public static class Builder {
		private GeoLocation onConstruction;

		public Builder() {
			this.onConstruction = new GeoLocation();
		}

		public GeoLocation build() {
			return this.onConstruction;
		}

		public GeoLocation.Builder withLatitude( final Double latitude ) {
			this.onConstruction.latitude = Objects.requireNonNull( latitude );
			return this;
		}

		public GeoLocation.Builder withLongitude( final Double longitude ) {
			this.onConstruction.longitude = Objects.requireNonNull( longitude );
			return this;
		}
	}
}

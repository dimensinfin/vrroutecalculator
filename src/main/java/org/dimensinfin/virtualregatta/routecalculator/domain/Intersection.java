package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.util.Objects;

/**
 * One <code>Intersection</code> represents a boundary crossing at a recorded location and following a direction. The direction is not a 360 degree
 * value but a predefined general direction from the rose wind.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class Intersection {
	protected GeoLocation location;
	protected Directions direction;

	// - C O N S T R U C T O R S
	private Intersection() {
	}

	public Intersection( final GeoLocation location, final Directions direction ) {
		this.location = location;
		this.direction = direction;
	}

	public Directions getDirection() {
		return this.direction;
	}

	public void setDirection( final Directions direction ) {
		this.direction = direction;
	}

	public GeoLocation getLocation() {
		return this.location;
	}

	public void setLocation( final GeoLocation location ) {
		this.location = location;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "[Intersection " );
		buffer.append( "location=" ).append( this.location ).append( "," );
		buffer.append( "direction=" ).append( this.direction ).append( "]" );
		return buffer.toString();
	}

	// - B U I L D E R
	public static class Builder {
		private Intersection onConstruction;

		public Builder() {
			this.onConstruction = new Intersection();
		}

		public Intersection build() {
			Objects.requireNonNull( this.onConstruction.location );
			Objects.requireNonNull( this.onConstruction.direction );
			return this.onConstruction;
		}

		public Intersection.Builder withDirection( final Directions direction ) {
			this.onConstruction.direction = direction;
			return this;
		}

		public Intersection.Builder withLocation( final GeoLocation location ) {
			this.onConstruction.location = Objects.requireNonNull( location );
			return this;
		}
	}
}

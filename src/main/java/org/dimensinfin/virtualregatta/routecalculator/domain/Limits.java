package org.dimensinfin.virtualregatta.routecalculator.domain;

/**
 * A Limit is an special version of a <code>Boundary</code> but that affects only to the latitude or the longitude. The values define the
 * north-south or east-west limits of an space area.
 *
 * @author Dimensinfin Software Limited (dimensinfin.sotfware@gmail.com)
 * @since 0.4.7
 */
public class Limits {
	public static final String NORTH_SOUTH = "NS";
	public static final String EAST_WEST = "EW";

	private String type = Limits.NORTH_SOUTH;
	private double westSouthlimit = 0.0;
	private double eastNorthLimit = 0.0;

	public Limits( final double left, final double right ) {
		westSouthlimit = left;
		eastNorthLimit = right;
	}

	public Limits( final String type, final double left, final double right ) {
		this.type = type;
		westSouthlimit = left;
		eastNorthLimit = right;
	}

	public double getEast() {
		return eastNorthLimit;
	}

	public double getNorth() {
		return eastNorthLimit;
	}

	public double getSouth() {
		return westSouthlimit;
	}

	public double getWest() {
		return westSouthlimit;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "[WindCell " );
		buffer.append( "type=" ).append( type ).append( "," );
		buffer.append( "WestSouth=" ).append( westSouthlimit ).append( "," );
		buffer.append( "EastNorth=" ).append( eastNorthLimit ).append( "]" );
		return buffer.toString();
	}
}

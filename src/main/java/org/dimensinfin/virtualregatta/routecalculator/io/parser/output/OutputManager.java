package org.dimensinfin.virtualregatta.routecalculator.io.parser.output;

import java.io.IOException;
import java.io.Writer;
import java.util.Objects;

import org.dimensinfin.virtualregatta.routecalculator.core.config.VORGConstants;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;

public class OutputManager {
	private Writer outputDestination;

	public void printHeader( final String header ) {
		try {
			this.outputDestination.write( header.concat( VORGConstants.NEWLINE ) );
			this.outputDestination.flush();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printRoute( final String tag, final Route route ) {
		try {
			final String outputHeader = "[" + tag + "]";
			final String routeBody = route.toPrinter();

			this.outputDestination.write( outputHeader.concat( VORGConstants.NEWLINE ) );
			this.outputDestination.write( routeBody.concat( VORGConstants.NEWLINE ) );
			this.outputDestination.flush();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//
//	private void send2VRTool( final Route awdRoute, final String title ) {
//		try {
//			final BufferedWriter writer = new BufferedWriter( new FileWriter( outputFile, true ) );
//
//			// - Compose the report.
//			final StringBuffer buffer = new StringBuffer();
//			buffer.append( VORGConstants.NEWLINE );
//			buffer.append( "O;Route;" ).append( title ).append( VORGConstants.NEWLINE );
//			buffer.append( "A;Color=$0000CCFF" ).append( VORGConstants.NEWLINE );
//			buffer.append( "A;Visible=TRUE" ).append( VORGConstants.NEWLINE );
//			buffer.append( "A;Linewidth=2" ).append( VORGConstants.NEWLINE );
//			buffer.append( "A;ShowText=TRUE" ).append( VORGConstants.NEWLINE );
//			buffer.append( "A;ShowBoat=FALSE" ).append( VORGConstants.NEWLINE );
//			buffer.append( awdRoute.vrtoolReport() );
//			buffer.append( VORGConstants.NEWLINE );
//			writer.write( buffer.toString() );
//			writer.close();
//		} catch (final IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	// TODO end of movement of code

	// -  B U I L D E R
	public static class Builder {
		private OutputManager onConstruction;

		public Builder() {
			this.onConstruction = new OutputManager();
		}

		public OutputManager build() {
			return this.onConstruction;
		}

		public OutputManager.Builder withDestination( final Writer outputDestination ) {
			this.onConstruction.outputDestination = Objects.requireNonNull( outputDestination );
			return this;
		}
	}
}

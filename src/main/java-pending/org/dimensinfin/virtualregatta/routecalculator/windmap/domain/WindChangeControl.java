package org.dimensinfin.virtualregatta.routecalculator.windmap.domain;

import java.util.Calendar;

import org.dimensinfin.virtualregatta.routecalculator.config.VORGConstants;
import org.dimensinfin.virtualregatta.routecalculator.domain.Directions;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Limits;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteCell;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteControl;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;

public class WindChangeControl extends RouteControl {
	private double	beforeElapsed;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WindChangeControl( final GeoLocation location, final Directions direction, final RouteCell leftNode,
	                          final RouteCell rightNode) {
		super(location, direction, leftNode, rightNode);
	}

	public WindChangeControl(final GeoLocation controlLocation, final Directions direction, final RouteCell preRoute,
			final RouteCell postRoute, final double elapsed) {
		super(controlLocation, direction, preRoute, postRoute);
		beforeElapsed = elapsed;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Limits getLatitudeLimits() {
		return left.getCell().getCeiling();
	}

	public Limits getLongitudeLimits() {
		return left.getCell().getWalls();
	}

	/** Changes the control latitude until the left time reaches the wind change time. */
	public double moveLatitude(final double targetLongitude) {
		final GeoLocation targetLocation = getCurrentLocation();

		// - Time to change from start point.
		final Limits latLimits = getLatitudeLimits();
		final double minutesBefore11 = nextElevenDifference(beforeElapsed);
		if (minutesBefore11 < 0) {
			// - Fix this position inside the control.
			storeLocation(targetLocation);
			upLocation.setLat(targetLocation.getLat());
			downLocation.setLat(targetLocation.getLat());
			return targetLocation.getLat();
		}
		final double currentTTC = left.getTTC() * 60.0;
		double targetAdjust = Math.abs(minutesBefore11 - currentTTC);

		// - Optimization. If found do not search
		if (targetAdjust < 1) {
			// - Fix this position inside the control.
			storeLocation(targetLocation);
			upLocation.setLat(targetLocation.getLat());
			downLocation.setLat(targetLocation.getLat());
			return targetLocation.getLat();
		}

		// - Get the new TTC for the previous longitude and next longitude.
		double newLatitude = Math.round((targetLocation.getLat() - Route.ITERATION_INCREMENT) * 100.0) / 100.0;
		targetLocation.setLat(newLatitude);
		left.setExitLocation(targetLocation);
		double newTTC = left.getTTC() * 60.0;
		double newAdjust = Math.abs(minutesBefore11 - newTTC);

		// - Optimization. If found do not search
		if (targetAdjust < 1) {
			// - Fix this position inside the control.
			storeLocation(targetLocation);
			upLocation.setLat(newLatitude);
			downLocation.setLat(newLatitude);
			return newLatitude;
		}
		if (newAdjust < targetAdjust)
			while (newLatitude >= latLimits.getSouth()) {
				targetAdjust = newAdjust;
				newLatitude -= Route.ITERATION_INCREMENT;

				targetLocation.setLat(newLatitude);
				left.setExitLocation(targetLocation);
				newTTC = left.getTTC() * 60.0;
				newAdjust = Math.abs(minutesBefore11 - newTTC);

				if (newAdjust <= 2.0) {
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLat(newLatitude);
					downLocation.setLat(newLatitude);
					return newLatitude;
				}
				// - Check if we have passed the optimum point.
				if (newAdjust > targetAdjust) {
					newLatitude -= Route.ITERATION_INCREMENT;
					targetLocation.setLat(newLatitude);
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLat(newLatitude);
					downLocation.setLat(newLatitude);
					return newLatitude;
				}
			}
		else
			while (newLatitude <= latLimits.getNorth()) {
				targetAdjust = newAdjust;
				newLatitude += Route.ITERATION_INCREMENT;

				targetLocation.setLat(newLatitude);
				left.setExitLocation(targetLocation);
				newTTC = left.getTTC() * 60.0;
				newAdjust = Math.abs(minutesBefore11 - newTTC);

				if (newAdjust <= 2.0) {
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLat(newLatitude);
					downLocation.setLat(newLatitude);
					return newLatitude;
				}
				// - Check if we have passed the optimum point.
				if (newAdjust > targetAdjust) {
					newLatitude -= Route.ITERATION_INCREMENT;
					targetLocation.setLat(newLatitude);
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLat(newLatitude);
					downLocation.setLat(newLatitude);
					return newLatitude;
				}
			}

		return Double.POSITIVE_INFINITY;
	}

	/** Changes the control longitude until the left time reaches the wind change time. */
	public double moveLongitude(final double targetLatitude) {
		final GeoLocation targetLocation = getCurrentLocation();
		left.setExitLocation(targetLocation);

		// - Time to change from start point.
		final Limits lonLimits = getLongitudeLimits();
		final double minutesBefore11 = nextElevenDifference(beforeElapsed);
		if (minutesBefore11 < 0) {
			// - Fix this position inside the control.
			storeLocation(targetLocation);
			upLocation.setLon(targetLocation.getLon());
			downLocation.setLon(targetLocation.getLon());
			return targetLocation.getLon();
		}
		final double currentTTC = left.getTTC() * 60.0;
		// - Optimization. If the TTC is Infinity then we can discard this latitude value.
		if (currentTTC == Double.POSITIVE_INFINITY) return Double.POSITIVE_INFINITY;
		double targetAdjust = Math.abs(minutesBefore11 - currentTTC);

		// - Optimization. If found do not search
		if (targetAdjust < 1) {
			// - Fix this position inside the control.
			storeLocation(targetLocation);
			upLocation.setLon(targetLocation.getLon());
			downLocation.setLon(targetLocation.getLon());
			return targetLocation.getLon();
		}

		// - Get the new TTC for the previous longitude to test movement sense.
		double newLongitude = getFixedLon() - Route.ITERATION_INCREMENT;
		targetLocation.setLon(newLongitude);
		left.setExitLocation(targetLocation);
		double newTTC = left.getTTC() * 60.0;
		double newAdjust = Math.abs(minutesBefore11 - newTTC);

		// - Optimization. If found do not search
		if (targetAdjust < 1) {
			// - Fix this position inside the control.
			storeLocation(targetLocation);
			upLocation.setLon(newLongitude);
			downLocation.setLon(newLongitude);
			return newLongitude;
		}
		if (newAdjust < targetAdjust)
			while (newLongitude >= lonLimits.getWest()) {
				targetAdjust = newAdjust;
				newLongitude -= Route.ITERATION_INCREMENT;

				targetLocation.setLon(newLongitude);
				left.setExitLocation(targetLocation);
				newTTC = left.getTTC() * 60.0;
				newAdjust = Math.abs(minutesBefore11 - newTTC);

				if (newAdjust < 2.0) {
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLon(newLongitude);
					downLocation.setLon(newLongitude);
					return newLongitude;
				}

				// - Check if we have passed the optimum point.
				if (newAdjust > targetAdjust) {
					newLongitude += Route.ITERATION_INCREMENT;
					targetLocation.setLon(newLongitude);
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLon(newLongitude);
					downLocation.setLon(newLongitude);
					return newLongitude;
				}
			}
		else
			while (newLongitude <= lonLimits.getEast()) {
				targetAdjust = newAdjust;
				newLongitude += Route.ITERATION_INCREMENT;

				targetLocation.setLon(newLongitude);
				left.setExitLocation(targetLocation);
				newTTC = left.getTTC() * 60.0;
				newAdjust = Math.abs(minutesBefore11 - newTTC);

				if (newAdjust < 2.0) {
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLon(newLongitude);
					downLocation.setLon(newLongitude);
					return newLongitude;
				}

				// - Check if we have passed the optimum point.
				if (newAdjust > targetAdjust) {
					newLongitude -= Route.ITERATION_INCREMENT;
					targetLocation.setLon(newLongitude);
					// - Fix this position inside the control.
					storeLocation(targetLocation);
					upLocation.setLon(newLongitude);
					downLocation.setLon(newLongitude);
					return newLongitude;
				}
			}

		return Double.POSITIVE_INFINITY;
	}

	public void setElapsed(final double elapsed) {
		beforeElapsed = elapsed;
	}

	private double nextElevenDifference(final double elapsedDate) {
		final Calendar entryDate = Calendar.getInstance();
		// final Calendar entryDate = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		final int minutes = new Double(elapsedDate * VORGConstants.TOMINUTES).intValue();
		entryDate.add(Calendar.MINUTE, minutes);
		final int entryHours = entryDate.get(Calendar.HOUR_OF_DAY);
		final Calendar elevenTime = Calendar.getInstance();
		// final Calendar elevenTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		if (entryHours < 11)
			elevenTime.set(elevenTime.get(Calendar.YEAR), elevenTime.get(Calendar.MONTH), elevenTime.get(Calendar.DATE), 11,
					0, 0);
		else
			elevenTime.set(elevenTime.get(Calendar.YEAR), elevenTime.get(Calendar.MONTH), elevenTime.get(Calendar.DATE), 23,
					0, 0);
		final long diff = elevenTime.getTimeInMillis() - entryDate.getTimeInMillis();
		// - Convert to minutes.
		final double mins = diff / (60 * 1000);
		return mins;
	}

	@Override
	public boolean adjustUp(int controlId) {
		if (Directions.NS == direction) {
			// FIXME Move the up location one not up.
			GeoLocation targetLocation = new GeoLocation(upLocation.getLat() - Route.ITERATION_INCREMENT, upLocation.getLon());
			final Limits iterationLimits = getLimits();
			if (targetLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ targetLocation.formattedLocation().replace('\t', ' '));
			storeLocation(targetLocation);
			// this.updateLeft(targetLocation);
			// this.updateRight(targetLocation);
			upLocation = targetLocation;
			// location = upLocation;
			moveLongitude(location.getLon());

			double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId);
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one not left.
			upLocation.setLon(upLocation.getLon() - Route.ITERATION_INCREMENT);
			final Limits iterationLimits = getLimits();
			if (upLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ upLocation.formattedLocation().replace('\t', ' '));
			storeLocation(upLocation);
			// this.updateLeft(upLocation);
			// this.updateRight(upLocation);
			// location = upLocation;
			moveLongitude(location.getLon());

			double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId);
			return true;
		}
		return false;
	}

	@Override
	public boolean adjustDown(int controlId) {
		if (Directions.NS == direction) {
			// FIXME Move the up location one not up.
			GeoLocation targetLocation = new GeoLocation(downLocation.getLat() + Route.ITERATION_INCREMENT, downLocation
					.getLon());
			final Limits iterationLimits = getLimits();
			if (targetLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ targetLocation.formattedLocation().replace('\t', ' '));
			storeLocation(targetLocation);
			// this.updateLeft(targetLocation);
			// this.updateRight(targetLocation);
			downLocation = targetLocation;
			// location = downLocation;
			moveLatitude(location.getLat());

			double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustDown(controlId);
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one not left.
			downLocation.setLon(downLocation.getLon() + Route.ITERATION_INCREMENT);
			final Limits iterationLimits = getLimits();
			if (downLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ downLocation.formattedLocation().replace('\t', ' '));
			storeLocation(downLocation);
			// this.updateLeft(downLocation);
			// this.updateRight(downLocation);
			// location = downLocation;
			moveLatitude(location.getLat());

			double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustDown(controlId);
			return true;
		}
		return false;
	}
}
// - UNUSED CODE ............................................................................................

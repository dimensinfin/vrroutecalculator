package org.dimensinfin.virtualregatta.routecalculator.windmap.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.dimensinfin.virtualregatta.routecalculator.LocationNotInMap;
import org.dimensinfin.virtualregatta.routecalculator.config.TimeZoneAdapter;
import org.dimensinfin.virtualregatta.routecalculator.config.VORGConstants;
import org.dimensinfin.virtualregatta.routecalculator.domain.Directions;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteCell;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;

public class WindChangeCell extends RouteCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected RouteCell							preRoute;
	protected RouteCell							postRoute;
	// private double beforeElapsed;
	private final WindChangeControl	shiftControl;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// public WindChangeCell() {
	// }
	/**
	 * Construct a WindChange from a simple RouteCell cell. Needs to update the list of cells to control the
	 * wind shift
	 * 
	 * @param elapsed
	 * @throws LocationNotInMap
	 */
	public WindChangeCell(final RouteCell reference, final double elapsed) throws LocationNotInMap {
		// this(reference.getCell(), reference.getEntryLocation(), reference.getExitLocation(), elapsed);
		// DEBUG The reference wind cell is not updated to the right wind cell. Not valid.
		// Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
		final Calendar now = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		now.add(Calendar.HOUR, -2);
		int minutes = new Double(elapsed * VORGConstants.TOMINUTES).intValue();
		now.add(Calendar.MINUTE, minutes);

		WindCell windCell = WindMapHandler.cellWithPoint(reference.getCell().getLocation(), now.getTime());
		// WindCell windCell = reference.getCell();
		GeoLocation entry = reference.getEntryLocation();
		GeoLocation exit = reference.getExitLocation();

		// - Calculate an intermediate initial point.
		GeoLocation midPoint = calculateMidPoint(entry, exit, nextElevenDifference(elapsed), reference.getSpeed());
		preRoute = new RouteCell(windCell, entry, midPoint);
		// Calendar entryTime = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
		// entryTime.add(Calendar.MINUTE, minutes);
		// Calendar endTime = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
		// endTime.add(Calendar.MINUTE, minutes);
		// endTime.add(Calendar.MINUTE, new Double(preRoute.getTTC() * 60.0).intValue());
		// System.out.println("Entry time=" + entryTime.getTime());
		// System.out.println("Exit time=" + endTime.getTime());

		// - Locate the next future wind cell.
		final WindCell currentWindCell = preRoute.getCell();
		final Date currentTime = currentWindCell.getTimeStamp();
		final Calendar futureTime = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
		futureTime.setTime(currentTime);
		futureTime.add(Calendar.HOUR, 13);
		final WindCell futureWindCell = WindMapHandler.cellWithPoint(currentWindCell.getLocation(), futureTime.getTime());
		postRoute = new RouteCell(futureWindCell, midPoint, exit);

		final Directions direction = calculateControlDirection(entry, exit);
		shiftControl = new WindChangeControl(midPoint, direction, preRoute, postRoute, elapsed);

		// - Update the control position to set the change just in the wind change time.
		// if (direction == Directions.EW) shiftControl.moveLatitude(0.0);
		// if (direction == Directions.NS) shiftControl.moveLongitude(0.0);
		// beforeElapsed = elapsed;
		// shiftControl.setElapsed(elapsed);

		// // - Perform the calculations for all the other cell parameters.
		// this.calculateCell();
	}

	private double nextElevenDifference(final double elapsedDate) {
		final Calendar entryDate = Calendar.getInstance();
		// final Calendar entryDate = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		final int minutes = new Double(elapsedDate * VORGConstants.TOMINUTES).intValue();
		entryDate.add(Calendar.MINUTE, minutes);
		final int entryHours = entryDate.get(Calendar.HOUR_OF_DAY);
		final Calendar elevenTime = Calendar.getInstance();
		// final Calendar elevenTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		if (entryHours < 11)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 11, 0,
					0);
		else if (entryHours < 23)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 23, 0,
					0);
		else
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE) + 1, 11,
					0, 0);
		final long diff = elevenTime.getTimeInMillis() - entryDate.getTimeInMillis();
		// - Convert to minutes.
		final double mins = diff / (60 * 1000);
		return mins;
	}

	// /**
	// * Create a new wind cell that contains two wind data sources. One the current and the next wind data box.
	// * Calculations inside this cell take care of the handling of both cells.
	// *
	// * @param beforeElapsed2
	// *
	// * @throws LocationNotInMap
	// */
	// protected WindChangeCell(final WindCell windCell, final GeoLocation entry, final GeoLocation exit, double
	// elapsed)
	// throws LocationNotInMap {
	// // - Calculate an intermediate initial point.
	// final GeoLocation midPoint = this.calculateMidPoint(entry, exit);
	// preRoute = new ProxyCell(windCell, entry, midPoint);
	//
	// // - Locate the next future wind cell.
	// final WindCell currentWindCell = preRoute.getCell();
	// final Date currentTime = currentWindCell.getTimeStamp();
	// final Calendar futureTime = Calendar.getInstance();
	// futureTime.setTime(currentTime);
	// futureTime.add(Calendar.HOUR, 13);
	// final WindCell futureWindCell = WindMapHandler.cellWithPoint(currentWindCell.getLocation(),
	// futureTime.getTime());
	// postRoute = new ProxyCell(futureWindCell, midPoint, exit);
	//
	// final Directions direction = this.calculateControlDirection(entry, exit);
	// shiftControl = new WindChangeControl(midPoint, direction, preRoute, postRoute, elapsed);
	//
	// // - Update the control position to set the change just in the wind change time.
	// if (direction == Directions.EW) shiftControl.moveLatitude(0.0);
	// if (direction == Directions.NS) shiftControl.moveLongitude(0.0);
	// }

	// - M E T H O D - S E C T I O N ..........................................................................
	private Directions calculateControlDirection(final GeoLocation entry, final GeoLocation exit) {
		final double dLat = exit.getLat() - entry.getLat();
		final double dLon = exit.getLon() - entry.getLon();
		if (dLat > dLon) return Directions.EW;
		return Directions.NS;
	}

	// private GeoLocation calculateMidPoint(final GeoLocation entry, final GeoLocation exit) {
	// final double dLat = exit.getLat() - entry.getLat();
	// final double dLon = exit.getLon() - entry.getLon();
	// return new GeoLocation(entry.getLat() + dLat / 2.0, entry.getLon() + dLon / 2.0);
	// }

	/**
	 * Calculate the point for a wind change. The angle is the angle between the start and end. The distance to
	 * run depends on the speed and the time to elapse until the wind change time. Adjust the longitude with the
	 * cosine of the latitude
	 */
	private GeoLocation calculateMidPoint(final GeoLocation start, final GeoLocation end, double minutes, double speed) {
		double angle = start.angleTo(end);
		// // - Adjust the distance as the proportional part to be run for the elapse time to run.
		// double Ls = Math.toRadians(start.getLat());
		// double Ld = Math.toRadians(end.getLat());
		// double deltaLon = end.getLon() - start.getLon();
		// double ldelta = Math.toRadians(deltaLon);
		// double loxDistance = VORGConstants.EARTHRADIUS
		// * Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		// double expectedTTC = loxDistance / speed;
		// double proportionalDistance = (minutes / 60.0) * loxDistance / expectedTTC;

		double distance = minutes / VORGConstants.TOMINUTES * speed;
		double travelLat = Math.cos(Math.toRadians(angle)) * distance;
		double travelLon = Math.sin(Math.toRadians(angle)) * distance;
		double medianLat = (start.getLat() + end.getLat()) / 2.0;
		travelLon = Math.cos(Math.toRadians(medianLat)) * travelLon;
		GeoLocation lastLocation = new GeoLocation(start.getLat() + travelLat / VORGConstants.TOMINUTES, start.getLon()
				+ travelLon / VORGConstants.TOMINUTES);
		return lastLocation;
	}

	public RouteCell getLeft() {
		return preRoute;
	}

	public RouteCell getRight() {
		return postRoute;
	}

	public WindChangeControl getControl() {
		return shiftControl;
	}

	// @Override
	// public void optimize() {
	// this.calculateCell();
	// // return getTTC();
	// }
	//
	// @Override
	// public String toString() {
	// final StringBuffer buffer = new StringBuffer("\n[WindChangeCell ");
	// buffer.append("beforeTime=").append(beforeElapsed).append("");
	// buffer.append("\npreRoute=").append(preRoute).append("");
	// buffer.append("\npostRoute=").append(postRoute).append("");
	// buffer.append("\ncontrol=").append(shiftControl).append("");
	// buffer.append("]");
	// return buffer.toString();
	// }
	//
	// private void calculateCell() {
	// // //- Cell calculation is a complex task. Evaluate both cells for all intermediate shift values.
	// // if (shiftControl.getDirection() == Directions.E) this.processLongitude();
	// // if (shiftControl.getDirection() == Directions.N) this.processLatitude();
	// this.processLatitude();
	// }
	//
	//
	// private double calculateRouteTime(final GeoLocation newLocation) {
	// shiftControl.storeLocation(newLocation);
	// final double leftTTC = shiftControl.getLeftTTC();
	// if (Double.POSITIVE_INFINITY == leftTTC) return Double.POSITIVE_INFINITY;
	// final double rigthTTC = shiftControl.getRightTTC();
	// if (Double.POSITIVE_INFINITY == rigthTTC) return Double.POSITIVE_INFINITY;
	// return leftTTC + rigthTTC;
	// }
	//
	// /**
	// * Checks the TTC for each possible value in the latitude axis to get the shortest path. The opposite
	// axis,
	// * longitude is moved to adjust to the wind change time boundary.
	// */
	// private double processLatitude() {
	// final Limits iterationLimits = shiftControl.getLimits();
	// double initialLon = shiftControl.getOptimizedLocation().getLon();
	// final double startLatitude = Math.round(shiftControl.getOptimizedLocation().getLat() * 100.0) / 100.0;
	// double bestTime = Double.POSITIVE_INFINITY;
	// final long iterations = 100;
	// for (int counter = 0; counter < iterations; counter++) {
	// // for (double controlRange = iterationLimits.getSouth(); controlRange <= iterationLimits.getNorth();
	// // controlRange += Route.ITERATION_INCREMENT) {
	// double controlRange = startLatitude - Route.ITERATION_INCREMENT * counter;
	// if (controlRange < iterationLimits.getWest()) controlRange = iterationLimits.getWest();
	// // - The control may move in both directions to adjust the change time point.
	// initialLon = shiftControl.moveLongitude(controlRange);
	// if (initialLon != Double.POSITIVE_INFINITY) {
	// GeoLocation newLocation = new GeoLocation(controlRange, initialLon);
	// double currentElapsed = this.calculateRouteTime(newLocation);
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) bestTime = currentElapsed;
	// }
	// // if (RouteCalculator.onDebug()) {
	// // System.out.println("Wind Shift CTRL=" + currentElapsed + " - " +
	// // shiftControl.left.getCell().toString());
	// // }
	//
	// // - Calculate the control position for the starboard side.
	// // iterationCounter++;
	// controlRange = startLatitude + Route.ITERATION_INCREMENT * counter;
	// if (controlRange > iterationLimits.getEast()) controlRange = iterationLimits.getEast();
	// initialLon = shiftControl.moveLongitude(controlRange);
	// if (initialLon != Double.POSITIVE_INFINITY) {
	// GeoLocation newLocation = new GeoLocation(controlRange, initialLon);
	// double currentElapsed = this.calculateRouteTime(newLocation);
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) bestTime = currentElapsed;
	// }
	// // if (RouteCalculator.onDebug()) {
	// // System.out.println("Wind Shift CTRL=" + currentElapsed + " - " +
	// // shiftControl.left.getCell().toString());
	// // }
	// }
	// return bestTime;
	// }
	//
	// /**
	// * Checks the TTC for each possible value in the latitude axis to get the shortest path. The opposite
	// axis,
	// * longitude is moved to adjust to the wind change time boundary.
	// */
	// private double processLongitude() {
	// final Limits iterationLimits = shiftControl.getLimits();
	// double initialLat = shiftControl.getOptimizedLocation().getLat();
	// final double startLongitude = shiftControl.getOptimizedLocation().getLon();
	// double bestTime = Double.POSITIVE_INFINITY;
	// final long iterations = 100;
	// // final boolean westFound = false;
	// // final boolean eastFound = false;
	// // - Move the latitude up and down until found the best solution.
	// for (int counter = 0; counter < iterations; counter++) {
	// double controlRange = startLongitude - Route.ITERATION_INCREMENT * counter;
	// if (controlRange < iterationLimits.getSouth()) controlRange = iterationLimits.getSouth();
	// // - The control may move in both directions to adjust the change time point.
	// initialLat = shiftControl.moveLatitude(controlRange);
	// if (initialLat == Double.POSITIVE_INFINITY) continue;
	// GeoLocation newLocation = new GeoLocation(initialLat, controlRange);
	// double currentElapsed = this.calculateRouteTime(newLocation);
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) bestTime = currentElapsed;
	// // if (RouteCalculator.onDebug()) {
	// // System.out.println("Wind Shift CTRL=" + currentElapsed + " - " +
	// // shiftControl.left.getCell().toString());
	// // }
	//
	// // - Calculate the control position for the starboard side.
	// // iterationCounter++;
	// controlRange = startLongitude + Route.ITERATION_INCREMENT * counter;
	// if (controlRange > iterationLimits.getNorth()) controlRange = iterationLimits.getNorth();
	// initialLat = shiftControl.moveLatitude(controlRange);
	// if (initialLat == Double.POSITIVE_INFINITY) continue;
	// newLocation = new GeoLocation(initialLat, controlRange);
	// currentElapsed = this.calculateRouteTime(newLocation);
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) bestTime = currentElapsed;
	// // if (RouteCalculator.onDebug()) {
	// // System.out.println("Wind Shift CTRL=" + currentElapsed + " - " +
	// // shiftControl.left.getCell().toString());
	// // }
	// }
	// return bestTime;
	//
	// // final Limits iterationLimits = control.getLimits();
	// // final double fixedLon = control.getFixedLon();
	// // - Calculate the control position for the port side.
	// // iterationCounter++;
	// // }
	//
	// }
	//

	//
	// public WindChangeCell(double elapsed, GeoLocation entry, GeoLocation exit) {
	// // - Get the two cells that conform the wind shift.
	// Calendar elapsedDate = GregorianCalendar.getInstance();
	// int seconds = new Double(elapsed * 3600.0).intValue();
	// elapsedDate.add(Calendar.SECOND, seconds);
	// try {
	// WindCell preWindCell = WindMapHandler.cellWithPoint(entry, elapsedDate.getTime());
	// globalRoute = new RouteCell(preWindCell, entry, exit);
	// double timeToEleven = nextElevenDifference(elapsedDate);
	//
	// // - Optimization No1. If the time for a wind shift is less than 1 hour no dot optimize.
	// if (timeToEleven <= 1) {
	// optimize = false;
	// return;
	// }
	//
	// // - Calculate new coordinate for this leg.
	// GeoLocation newExit = calculatePosition(entry, exit, timeToEleven);
	// preRoute = new ProxyCell(preWindCell, entry, newExit);
	//
	// seconds = new Double(globalRoute.getTTC() * 3600.0).intValue();
	// elapsedDate.add(Calendar.SECOND, seconds);
	// WindCell postWindCell = WindMapHandler.cellWithPoint(entry, elapsedDate.getTime());
	// postRoute = new ProxyCell(postWindCell, newExit, exit);
	//
	// shiftControl = new RouteControl(new GeoLocation(newExit.getLat(), newExit.getLon()),
	// calculateControlDirection(
	// entry, exit), preRoute, postRoute);
	// } catch (LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
}

// class z {
//
// private GeoLocation calculatePosition(GeoLocation start, GeoLocation end, double timeToEleven) {
// double angle = start.angleTo(end);
// // - Calculate location from start.
// double dLat = end.getLat() - start.getLat();
// double dLon = end.getLon() - start.getLon();
// double hyp = Math.hypot(start.getLat() - end.getLat(), start.getLon() - end.getLon());
// // - Movement units per hour.
// double ttc = globalRoute.getTTC();
// double rad = timeToEleven / globalRoute.getTTC();
// double diffLat = dLat * rad;
// double diffLon = dLon * rad;
// GeoLocation newLocation = new GeoLocation(start.getLat() + diffLat, start.getLon() + diffLon);
// return newLocation;
// }
//
// // private double nextElevenDifference(Calendar elapsedDate) {
// // int elapsedHours = elapsedDate.get(Calendar.HOUR_OF_DAY);
// // Calendar elevenTime;
// // if (elapsedHours < 11) {
// // elevenTime = GregorianCalendar.getInstance();
// // elevenTime.set(elevenTime.get(Calendar.YEAR), elevenTime.get(Calendar.MONTH),
// elevenTime.get(Calendar.DATE), 11,
// // 0, 0);
// // } else {
// // elevenTime = GregorianCalendar.getInstance();
// // elevenTime.set(elevenTime.get(Calendar.YEAR), elevenTime.get(Calendar.MONTH),
// elevenTime.get(Calendar.DATE), 23,
// // 0, 0);
// // }
// // long diff = elevenTime.getTimeInMillis() - elapsedDate.getTimeInMillis();
// // // - Convert to hours.
// // double hours = diff / (60 * 60 * 1000);
// // return hours;
// // }
//
// // private double optimize(double ttc) {
// // if (optimize) {
// // if (shiftControl.getDirection() == Directions.E)
// // return processLatitude();
// // else
// // return processLongitude();
// // } else
// // return ttc;
// // }
//
//
// }

// - UNUSED CODE ............................................................................................

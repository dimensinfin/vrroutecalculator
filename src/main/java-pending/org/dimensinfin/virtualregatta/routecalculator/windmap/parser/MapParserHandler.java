package org.dimensinfin.virtualregatta.routecalculator.windmap.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindCell;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindMap;

public class MapParserHandler extends DefaultHandler {
	private static final double					KN2KNOTS		= 0.53995680346;

	protected Map<Date, WindMap> mapTarget;
	private WindMap											currentMap	= null;

	public MapParserHandler(final Map<Date, WindMap> windMaps) {
		this.mapTarget = windMaps;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		if (name.equals("PREVISION")) {
			// - Convert the reference to a Date for boundary management.
			String mapRef = attributes.getValue("DATE");
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date mapDate = df.parse(mapRef);

				// - Check if we have registered a map with that reference.
				currentMap = mapTarget.get(mapDate);
				if (null == currentMap) {
					currentMap = new WindMap();
					currentMap.setRef(mapRef);
					currentMap.setTimeStamp(mapDate);
					mapTarget.put(mapDate, currentMap);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("M")) {
			if (null == currentMap) return;
			String lat = attributes.getValue("LAT");
			String lon = attributes.getValue("LON");
			String heading = attributes.getValue("D");
			double speed = Double.parseDouble(attributes.getValue("V")) * KN2KNOTS;
			// speed = Math.round(speed * 10.0) / 10.0;
			currentMap.addCell(new WindCell(Integer.parseInt(lat), Integer.parseInt(lon), Integer.parseInt(heading), speed));
		}
	}
}

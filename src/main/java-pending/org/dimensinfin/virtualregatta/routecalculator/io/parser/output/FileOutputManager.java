package org.dimensinfin.virtualregatta.routecalculator.io.parser.output;

import java.util.Objects;

public class FileOutputManager implements IOutputManager {
	private String outputFileName;

	private FileOutputManager() {}

	// - B U I L D E R
	public static class Builder {
		private FileOutputManager onConstruction;

		public Builder() {
			this.onConstruction = new FileOutputManager();
		}

		public FileOutputManager build() {
			return this.onConstruction;
		}

		public FileOutputManager.Builder withOutputFileName( final String outputFileName ) {
			this.onConstruction.outputFileName = Objects.requireNonNull( outputFileName );
			return this;
		}
	}
}

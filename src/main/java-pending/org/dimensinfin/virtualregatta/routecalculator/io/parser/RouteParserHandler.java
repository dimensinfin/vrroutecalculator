package org.dimensinfin.virtualregatta.routecalculator.io.parser;

import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.dimensinfin.virtualregatta.routecalculator.domain.ExtendedRoute;
import org.dimensinfin.virtualregatta.routecalculator.router.Router;

public class RouteParserHandler extends DefaultHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger	= Logger.getLogger("net.sf.vorg.routecalculator.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	private final Router routerRef;
	private final ExtendedRoute route;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteParserHandler(final Router router, final ExtendedRoute directRoute) {
		routerRef = router;
		route = directRoute;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes attributes)
			throws SAXException {
		// - Store the information for the user account for login purposes
		if (name.toLowerCase().equals("route")) route.setName(attributes.getValue("name"));
		if (name.toLowerCase().equals("waypointlist")) routerRef.startElement(name, attributes, route);
		if (name.toLowerCase().equals("waypoint")) routerRef.startElement(name, attributes, route);
	}
}

package org.dimensinfin.virtualregatta.routecalculator.domain;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.xml.sax.SAXException;
import org.dimensinfin.virtualregatta.routecalculator.LocationNotInMap;
import org.dimensinfin.virtualregatta.routecalculator.domain.Directions;
import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.domain.Intersection;
import org.dimensinfin.virtualregatta.routecalculator.domain.Route;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteCell;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteControl;
import org.dimensinfin.virtualregatta.routecalculator.domain.RouteState;
import org.dimensinfin.virtualregatta.routecalculator.windmap.WindMapHandler;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindCell;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Adds method for the creation of a route that dues not need to follow a straight line form the start to the
 * end. This allows to describe routes of irregular shape based on mid points waypoints that are generated or
 * processed outside the class. The main entry data point is then the <code>addWaypoint</code> method.<br>
 * It also adds a name identifier to the path so the external classes may work with several routes single
 * identified.
 */
public class ExtendedRoute extends Route {
	private static final double	TOSECONDS	= 60.0 * 60.0;

	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private String							name;
	// private GeoLocation startLocation;
	/** The last route waypoint reference. */
	private GeoLocation currentLocation;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ExtendedRoute() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * The creation of a route from a set of multiple waypoints is now a process that is performed inside the
	 * Route. The data comes in GeoLocations that have to be connected to detect the cells that are going to be
	 * part of the route and the entry and exit points for that cells. It will also deal with duplicated points
	 * and with points that are not bounded to a border with the exception of the initial and last route points.
	 * 
	 * @throws LocationNotInMap
	 * 
	 * @throws SAXException
	 */
	public void addWaypoint(final GeoLocation waypoint) throws LocationNotInMap, SAXException {
		// - Check if this is the first point being received.
		if (null == currentLocation) {
			// startLocation = waypoint;
			currentLocation = waypoint;
		} else {
			// - This is not the first point so connect the two points and create a direct route between them.
			Route newLeg = new Route();
			// - The current search time is the current time plus the elapsed current route time.
			final Calendar now = this.getSearchTime();
			// - Locate the wind cells that contains the start and end locations.
			// WindMapHandler.loadWinds(currentLocation);
			final WindCell startCell = WindMapHandler.cellWithPoint(currentLocation, now.getTime());
			final WindCell endCell = WindMapHandler.cellWithPoint(waypoint, now.getTime());

			// - Optimizations. If both points are on the same cell we have finished building the leg.
			if (endCell.isEquivalent(startCell)) {
				final Vector<Intersection> intersections = (Vector<Intersection>) startCell.calculateIntersection(currentLocation, waypoint);
				final Directions intersectionDirection = intersections.lastElement().getDirection();
				newLeg.add(startCell, intersectionDirection, currentLocation, waypoint);
			} else {
				// - Add all the cells to a new route that will become a leg on the main route.
				int seconds = new Double(this.getRouteTTC() * TOSECONDS).intValue();
				newLeg = this.calculateDirectRoute(currentLocation, waypoint, seconds);
			}

			// - Add current route cell to this route.
			this.appendRoute(newLeg);
			currentLocation = waypoint;
		}
	}

	@Override
	public void setName(final String newName) {
		name = newName;
	}

	private void appendRoute(final Route newLeg) throws SAXException, LocationNotInMap {
		if (creationState == RouteState.EMPTY) {
			// - Set this route to the leg route.
			route = newLeg.route;
			controls = newLeg.controls;
			creationState = RouteState.MIDDLE;
			this.lastNode = newLeg.route.lastElement();
		} else {
			// - Get the last cell of the current route and the first of the new leg and connect them.
			final RouteCell last = route.lastElement();
			final RouteCell first = newLeg.route.firstElement();

			// - Special case. The new leg only has a cell that is a new cell.
			if (newLeg.route.size() == 1) {
				// - The problem now is to know what is the direction.
				WindCell windCell = last.getCell();
				Vector<Intersection> intersections = (Vector<Intersection>) windCell.calculateIntersection(last.getEntryLocation(), first
						.getExitLocation());
				this.add(first, intersections.lastElement().getDirection());
			} else {
				// - End and initial locations should match because are the same waypoint.
				// so change the end point of the last cell by the end point of the first cell and suppress this one.
				GeoLocation endPoint = route.lastElement().getExitLocation();
				GeoLocation initialPoint = newLeg.route.firstElement().getEntryLocation();
				if (!endPoint.isEquivalent(initialPoint))
					throw new SAXException("Subleg connection does not math. Route generation is not valid.");
				final GeoLocation newLocation = first.getExitLocation();
				last.setExitLocation(newLocation);
				// - Update the last route reference for the construction of the control connections.
				lastNode = last;

				// - Add the rest of cells to the main route with the corresponding controls.
				final Iterator<RouteCell> rcit = newLeg.route.iterator();
				final Iterator<RouteControl> cit = newLeg.controls.iterator();
				while (rcit.hasNext()) {
					final RouteCell newCell = rcit.next();
					if (newCell.equals(first))
						continue;
					else {
						final RouteControl control = cit.next();
						this.add(newCell, control.getDirection());
					}
				}
			}
		}
	}

	/**
	 * Create a multiple or single cell route from the straight line that connect the start point to the end
	 * point. Keep elapsed time while building up the leg to identify the standard cells and the wind change
	 * cells.
	 * 
	 * @param start
	 *          initial location of the leg
	 * @param end
	 *          end location of the leg
	 * @param bias
	 *          number of seconds already elapsed on the previous cells of other routes where this one will be
	 *          connected.
	 * @return a new route leg
	 * @throws LocationNotInMap
	 */
	public Route calculateDirectRoute( final GeoLocation start, final GeoLocation end, final int bias )
			throws LocationNotInMap {
		// - Buildup the route to the end from the start cell.
		final Route directRoute = new Route();
		Calendar searchTime = directRoute.getSearchTime();
		searchTime.add(Calendar.SECOND, bias);

		// - Locate the wind cells that contains the start and end locations.
		// WindMapHandler.loadWinds(start);
		final WindCell startCell = WindMapHandler.cellWithPoint(start, searchTime.getTime());
		WindCell endCell = WindMapHandler.cellWithPoint(end, searchTime.getTime());

		Vector<Intersection> intersections = (Vector<Intersection>) startCell.calculateIntersection(start, end);
		directRoute.add(startCell, intersections.lastElement().getDirection(), start, intersections.lastElement()
				.getLocation());
		// - Recalculate leg building time to locate the wind cells and also identify which are changes.
		searchTime = directRoute.getSearchTime();
		searchTime.add(Calendar.SECOND, bias);

		// - Get cell one by one following the direct path line.
		GeoLocation nextLocation = startCell.cellAtDirection(intersections.lastElement().getDirection());
		WindCell nextCell = WindMapHandler.cellWithPoint(nextLocation, searchTime.getTime());
		// - The end cell is never reached because the cell maps change over time. Update it.
		while (!nextCell.isEquivalent(endCell)) {
			intersections = (Vector<Intersection>) nextCell.calculateIntersection(start, end);
			directRoute.add(nextCell, intersections.firstElement().getDirection(),
					intersections.firstElement().getLocation(), intersections.lastElement().getLocation());
			searchTime = directRoute.getSearchTime();
			searchTime.add(Calendar.SECOND, bias);
			nextLocation = nextCell.cellAtDirection(intersections.lastElement().getDirection());
			nextCell = WindMapHandler.cellWithPoint(nextLocation, searchTime.getTime());
			// nextCell = WindMapHandler.cellWithPoint(intersections.lastElement().getLocation(), nextCell,
			// now.getTime());
			endCell = WindMapHandler.cellWithPoint(end, searchTime.getTime());
		}
		// - Add the end cell of the leg already updated to the right wind cell.
		intersections = (Vector<Intersection>) endCell.calculateIntersection(start, end);
		directRoute.add(endCell, intersections.firstElement().getDirection(), intersections.firstElement().getLocation(),
				end);
		return directRoute;
	}
}

// - UNUSED CODE ............................................................................................

package org.dimensinfin.virtualregatta.routecalculator.domain;

public enum RouteState {
	EMPTY, MIDDLE, CLONED;
}

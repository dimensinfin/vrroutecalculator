package org.dimensinfin.virtualregatta.routecalculator.domain;

import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindCell;

public interface AbstractRouteCell {
	GeoLocation getEntryLocation();

	void setEntryLocation( GeoLocation entryLocation );

	GeoLocation getExitLocation();

	void setExitLocation( GeoLocation exitLocation );

	WindCell getCell();

	double getTTC();

	void setWindData( final WindCell windData );

	void optimize();

	String printReport( final int waypointId );

	String printStartReport();

	String vrtoolReport( int index );
}

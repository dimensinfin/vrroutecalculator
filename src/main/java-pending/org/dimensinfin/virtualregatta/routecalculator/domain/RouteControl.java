package org.dimensinfin.virtualregatta.routecalculator.domain;

public class RouteControl {
	protected GeoLocation location = new GeoLocation();
	protected Directions direction = Directions.NS;
	protected RouteCell left;
	protected RouteCell right;
	protected GeoLocation upLocation;
	protected GeoLocation downLocation;
	/**
	 * Stores the last optimized location to be used as the start point for next iterations.
	 */
	private GeoLocation optimizedLocation;
	private Limits controlLimits;

	/**
	 * Creates a new control that connect two route cells, setting a direction and the initial control location.
	 */
	public RouteControl( final GeoLocation location, final Directions setDirection, final RouteCell leftNode, final RouteCell rightNode ) {
		this.location = location;
		this.upLocation = location;
		this.downLocation = location;
		this.optimizedLocation = location;
		this.direction = this.adjustDirection( setDirection );
		this.left = leftNode;
		this.right = rightNode;
		if (this.direction == Directions.NS) this.controlLimits = this.left.getCell().getCeiling();
		if (this.direction == Directions.EW) this.controlLimits = this.left.getCell().getWalls();

		// - Register the control inside the route nodes.
		leftNode.registerControlLeft( this );
		rightNode.registerControlRight( this );
	}

	public GeoLocation getCurrentLocation() {
		return location;
	}
	public Directions getDirection() {
		return direction;
	}

	public double getFixedLon() {
		return left.getExitLocation().getLon();
	}

	public double getLeftTTC() {
		return left.getTTC();
	}

	public Limits getLimits() {
		return controlLimits;
	}

	public void setLeft( final RouteCell newLeft ) {
		left = newLeft;
		left.registerControlLeft( this );
	}

	public void setOptimizedLocation( final GeoLocation newLocation ) {
		optimizedLocation = newLocation;
	}

	public void setRight( final RouteCell newRigth ) {
		right = newRigth;
		right.registerControlRight( this );
	}

	/**
	 * Increments the downLocation for latitude or longitude depending on the control type until it reaches the
	 * cell limit. It this limit is reached the method returns <code>false</code> to avoid being called again.
	 */
	public boolean adjustDown( final int controlId ) {
		if (Directions.NS == direction) {
			// - Move the up location one step left.
			downLocation.setLat( downLocation.getLat() + Route.ITERATION_INCREMENT );
			final Limits iterationLimits = this.getLimits();
			if (downLocation.getLat() > iterationLimits.getNorth()) return false;
			if (controlId < 1)
				System.out.println( "Iterating level " + controlId + " with "
						+ downLocation.formattedLocation().replace( '\t', ' ' ) );
			this.storeLocation( downLocation );
			// this.updateLeft(downLocation);
			// this.updateRight(downLocation);
			// location = downLocation;

			final double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return this.adjustDown( controlId );
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one step left.
			downLocation.setLon( downLocation.getLon() + Route.ITERATION_INCREMENT );
			final Limits iterationLimits = this.getLimits();
			if (downLocation.getLon() > iterationLimits.getEast()) return false;
			if (controlId < 1)
				System.out.println( "Iterating level " + controlId + " with "
						+ downLocation.formattedLocation().replace( '\t', ' ' ) );
			this.storeLocation( downLocation );
			// this.updateLeft(downLocation);
			// this.updateRight(downLocation);
			// location = downLocation;

			final double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return this.adjustDown( controlId );
			return true;
		}
		return false;
	}

	/**
	 * Decrements the upLocation for latitude or longitude depending on the control type until it reaches the
	 * cell limit. It this limit is reached the method returns <code>false</code> to avoid being called again.
	 */
	public boolean adjustUp( final int controlId ) {
		if (Directions.NS == direction) {
			// FIXME Move the up location one step up.
			upLocation.setLat( upLocation.getLat() - Route.ITERATION_INCREMENT );
			final Limits iterationLimits = this.getLimits();
			if (upLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println( "Iterating level " + controlId + " with "
						+ upLocation.formattedLocation().replace( '\t', ' ' ) );
			this.storeLocation( upLocation );
			// this.updateLeft(upLocation);
			// this.updateRight(upLocation);
			// location = upLocation;

			final double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return this.adjustUp( controlId );
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one step left.
			upLocation.setLon( upLocation.getLon() - Route.ITERATION_INCREMENT );
			final Limits iterationLimits = this.getLimits();
			if (upLocation.getLon() < iterationLimits.getWest()) return false;
			if (controlId < 1)
				System.out.println( "Iterating level " + controlId + " with "
						+ upLocation.formattedLocation().replace( '\t', ' ' ) );
			this.storeLocation( upLocation );
			// this.updateLeft(upLocation);
			// this.updateRight(upLocation);
			// location = upLocation;

			final double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return this.adjustUp( controlId );
			return true;
		}
		return false;
	}

	/**
	 * Sets the start point for the up and down locations to the rounded value of the optimized location.
	 */
	public void reset() {
		upLocation = new GeoLocation( Math.round( optimizedLocation.getLat() * 100.0 ) / 100.0, Math.round( optimizedLocation
				.getLon() * 100.0 ) / 100.0 );
		downLocation = new GeoLocation( Math.round( optimizedLocation.getLat() * 100.0 ) / 100.0, Math.round( optimizedLocation
				.getLon() * 100.0 ) / 100.0 );
	}

	public void storeLocation( final GeoLocation newLocation ) {
		this.updateLeft( newLocation );
		this.updateRight( newLocation );
		location = newLocation;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "\n[RouteControl " );
		buffer.append( "direction=" ).append( direction ).append( ", " );
		buffer.append( "setup location=" ).append( location ).append( "" );
		buffer.append( "\n              " ).append( "optimizedLocation=" ).append( optimizedLocation ).append( "]" );
		return buffer.toString();
	}

	private Directions adjustDirection( final Directions setDirection ) {
		Directions adjustedDirection = setDirection;
		if (setDirection == Directions.N) adjustedDirection = Directions.EW; // Convert the input direction into a general NS or EW direction.
		if (setDirection == Directions.S) adjustedDirection = Directions.EW;
		if (setDirection == Directions.E) adjustedDirection = Directions.NS;
		if (setDirection == Directions.W) adjustedDirection = Directions.NS;
		return adjustedDirection;
	}

	private double updateLeft( final GeoLocation newLocation ) {
		left.setExitLocation( newLocation );
		return left.getTTC();
	}

	private double updateRight( final GeoLocation newLocation ) {
		right.setEntryLocation( newLocation );
		return right.getTTC();
	}
}

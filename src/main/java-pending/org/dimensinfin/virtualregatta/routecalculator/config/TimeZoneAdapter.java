package org.dimensinfin.virtualregatta.routecalculator.config;

import java.util.Calendar;
import java.util.TimeZone;

public class TimeZoneAdapter {
	public static Calendar changeTimeZone( Calendar targetDate, String newTimeZoneID ) {
		int localTimeZoneOffset = targetDate.getTimeZone().getOffset( targetDate.getTimeInMillis() );
		int gmtTimeZoneOffset = TimeZone.getTimeZone( newTimeZoneID ).getOffset( targetDate.getTimeInMillis() );
		int savings = targetDate.getTimeZone().getDSTSavings();
		int localDisplacement = localTimeZoneOffset * -1 + gmtTimeZoneOffset * -1;
		targetDate.add( Calendar.MILLISECOND, localTimeZoneOffset * -1 );
		targetDate.add( Calendar.MILLISECOND, gmtTimeZoneOffset * -1 );
		targetDate.add( Calendar.MILLISECOND, savings );
		return targetDate;
	}
}

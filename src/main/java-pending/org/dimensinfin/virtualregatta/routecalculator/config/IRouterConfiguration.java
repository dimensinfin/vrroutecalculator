package org.dimensinfin.virtualregatta.routecalculator.config;

public interface IRouterConfiguration {
	Integer getLeftDeviationAngle ();
	Integer getRightDeviationAngle ();
}

package org.dimensinfin.virtualregatta.routecalculator.config;

import org.dimensinfin.virtualregatta.routecalculator.boat.domain.SailConfiguration;

public interface VMCData {
	int getBestAngle();

	int getWorstAngle();

	SailConfiguration getBestSailConfiguration();

	int getMaxAWD();

	Object getMaxSpeed();

	void addLeftData( double leftVMC, int leftAngle, SailConfiguration leftConfiguration );

	void addRightData( double rightVMC, int rightAngle, SailConfiguration rightConfiguration );

	/**
	 * Scans all angles to store in the internal fields the values for the port, starboard and max speed VMC.
	 */
	void calculateVMC();

	String printRecord();

	String printReport();

	String toString();
}

package org.dimensinfin.virtualregatta.routecalculator.config;

import java.text.NumberFormat;
import java.util.Locale;

public class FormatSingletons {
	public static final NumberFormat nf1 = NumberFormat.getInstance( Locale.ENGLISH );
	public static final NumberFormat nf2 = NumberFormat.getInstance( Locale.ENGLISH );
	public static final NumberFormat nf3 = NumberFormat.getInstance( Locale.ENGLISH );
	public static final NumberFormat nf4 = NumberFormat.getInstance( Locale.ENGLISH );
	public static final NumberFormat nf5 = NumberFormat.getInstance( Locale.ENGLISH );

	static {
		nf1.setMaximumFractionDigits( 1 );
		nf1.setMinimumFractionDigits( 1 );
	}

	static {
		nf2.setMaximumFractionDigits( 2 );
		nf2.setMinimumFractionDigits( 2 );
	}

	static {
		nf3.setMaximumFractionDigits( 3 );
		nf3.setMinimumFractionDigits( 3 );
	}

	static {
		nf4.setMaximumFractionDigits( 4 );
		nf4.setMinimumFractionDigits( 4 );
	}

	static {
		nf5.setMaximumFractionDigits( 5 );
		nf5.setMinimumFractionDigits( 5 );
	}
}

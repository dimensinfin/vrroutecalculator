package org.dimensinfin.virtualregatta.routecalculator.core.exception;

import org.dimensinfin.virtualregatta.routecalculator.domain.GeoLocation;
import org.dimensinfin.virtualregatta.routecalculator.windmap.domain.WindCell;

public class InvalidCellIntersection extends RouteCalculatorException {

	public InvalidCellIntersection( final GeoLocation boatLocation, final WindCell windCell , final int direction) {
		super(ErrorInfo.INVALID_CELL_INTERSECTION_CALCULATION.getErrorMessage(
				windCell.getWalls().getNorth(),
				windCell.getWalls().getEast(),
				windCell.getWalls().getSouth(),
				windCell.getWalls().getWest(),
				boatLocation.getLat(),
				boatLocation.getLat(),
				direction)
		);
	}
}

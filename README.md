# Virtual Regatta - Route Calculator
## Sinopsis
Route calculation on offshore regattas is of most importance to reach 
the destination in a good ranking position. On long distance regattas
there is not a such approach as direct VMG best route since there are
obstacles than make the route a set of legs to be solve one by one.

Some of the intermediate points are not to be considered as a single
point but as to reach a predeterminate latitudo or longitude or pass
a mark point. So intermediate points are not easy targets since many
map cccordinates can satisfy the 'point reached' trigger.

So optimized long distance route calculation should not tie the route to
specific intermediate points so it should be wide to detect leg completion
events. This is also valid for subleg divisions when a leg is divided
into smaller pieces.

## Route definition
A basic route defines the path between a start point represented by a pair
of map coordinates usually in the form of navigation sexagesimal
degress minutes and seconds and a destination point also described with a
pair of coordinates.

Sail navigation depends on wind direction and intensity that may also change
over time and with location. So a simplification of a route definition is
a path the connects a start point with a end point traversing a set of 
wind cells that contain information about the wind speed and direction.

An optimal route will then be the VMG (velocity make good) projection of
the line that connects the start and end points taking on account the
wind conditions that may change the best route calculated.

An optimal route is one of the set of routes that starts at the start point
and ends at ot close to the end point (more about end conditions later)
but that the time elapsed to run the distance is the smaller time for all
the possible routes.

This route calculator will try to calculate the best route from the start
point to complete the end condition in the less time possible. It should
have on account for wind cells, wind cell data changes over time and the
boat polars to perform the sail changes at the righ places to get the
most of the boat speed VMG for each wind condition.

Leg definition is not one of the calculator goals. This should be performed
by another tool that should have on account for terrain and end conditions
matching.

## Tool Components
To calculate the route and display the results I will create a backend
rest tool that received the requests with an http request and that outputs
the result as a JSON formatted object.

Presentation of the results can be done on any navigator or by an specific
tool client developed in Angular and that can render the results.

Most of the use for the resulting data is to add it to VRTool graphical
tool than can draw the routes and some of the waypoint information over a
Earth land map projection.

## Domain elements
### Cell
It represents a rectangle area of the map where the wind intensity
and direction are constants and equal on all the points.
### Timed Wind Cell
A **Cell** valid for a determinate period of time. The time information
signals when the wind conditions start affecting the space and the
duration of such conditions. Time data is represented in GMT time
coordinates.
